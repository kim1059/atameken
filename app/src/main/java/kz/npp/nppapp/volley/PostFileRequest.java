package kz.npp.nppapp.volley;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kim1059 on 21.09.2016.
 */
public class PostFileRequest extends Request<String> {

    private MultipartEntity entity = new MultipartEntity();

    private String filePartName;
    private Map<String, String> headers = new HashMap<>();

    private final Response.Listener<String> mListener;
    private final File mFilePart;

    public PostFileRequest(int method, String url, File file, String partName,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(method, url, errorListener);

        mListener = listener;
        mFilePart = file;
        filePartName = partName;
        buildMultipartEntity();
    }

    private void buildMultipartEntity() {
        entity.addPart("body", new ByteArrayBody(convertFile(mFilePart), "file"));
        try {
            entity.addPart("file", new StringBody(filePartName));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void setCredentials(String login, String password){
        String creds = String.format("%s:%s",login,password);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        headers.put("Authorization", auth);
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String getBodyContentType()
    {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        }
        catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, "UTF-8");

            return Response.success(jsonString, getCacheEntry());
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(String response)
    {
        mListener.onResponse(response);
    }

    private byte[] convertFile(File file) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 11];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();

            Log.e("Byte array", ">" + byteArray);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.encode(byteArray, Base64.DEFAULT);
    }
}