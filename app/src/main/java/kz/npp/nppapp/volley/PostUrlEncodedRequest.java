package kz.npp.nppapp.volley;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kim1059 on 03.09.2016.
 */
public class PostUrlEncodedRequest extends StringRequest {

    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> params = new HashMap<>();

    public PostUrlEncodedRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public void setData(String formUUID, String uuid, String data){
        params.put("formUUID", formUUID);
        params.put("uuid", uuid);
        params.put("data", data);
    }

    public void setFileData(String parent, String path, String name){
        params.put("folderID", parent);
        params.put("path", path);
        params.put("name", name);
        Log.e("params", params.toString());
    }

    public void setCredentials(String login, String password){
        String creds = String.format("%s:%s",login,password);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        headers.put("Authorization", auth);
    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        Map<String, String> pars = new HashMap<String, String>();
        pars.put("Username", "usr");
        pars.put("Password", "passwd");
        pars.put("grant_type", "password");
        return params;
    }

    @Override
    public String getBodyContentType() {
        return String.format("application/x-www-form-urlencoded");
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

}
