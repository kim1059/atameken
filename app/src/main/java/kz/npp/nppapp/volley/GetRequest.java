package kz.npp.nppapp.volley;

import android.util.Base64;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.protocol.HTTP;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kim1059 on 01.05.2016.
 */
public class GetRequest extends StringRequest {

    private Map<String, String> headers = new HashMap<>();
    protected static final String TYPE_UTF8_CHARSET = "charset=UTF-8";
    private int mStatusCode;

    private Response.ErrorListener mErrorListener;

    public GetRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        mErrorListener = errorListener;
    }

    public void setCredentials(String login, String password){
        String creds = String.format("%s:%s",login,password);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        headers.put("Authorization", auth);
    }

    public void setXauthKey(String key){
        headers.put("X-Auth-Key", key);
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    protected Response<String> parseNetworkResponse(
            NetworkResponse response) {

        try {Log.e("status", "status");

            mStatusCode = response.statusCode;
            String type = response.headers.get(HTTP.CONTENT_TYPE);
            if (type == null) {
                Log.d("LOG", "content type was null");
                type = TYPE_UTF8_CHARSET;
                response.headers.put(HTTP.CONTENT_TYPE, type);
            } else if (!type.contains("UTF-8")) {
                Log.d("LOG", "content type had UTF-8 missing");
                type += ";" + TYPE_UTF8_CHARSET;
                response.headers.put(HTTP.CONTENT_TYPE, type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.parseNetworkResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        mErrorListener.onErrorResponse(error);
    }

    public int getStatusCode() {
        return mStatusCode;
    }

}
