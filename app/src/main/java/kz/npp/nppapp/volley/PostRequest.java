package kz.npp.nppapp.volley;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kim1059 on 01.05.2016.
 */
public class PostRequest extends StringRequest {

    private Map<String, String> headers = new HashMap<>();
    private String value;

    public PostRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public void setData(String value){
        this.value = value;
        Log.e("value", value);
    }

    public void setCredentials(String login, String password){
        String creds = String.format("%s:%s",login,password);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        headers.put("Authorization", auth);
    }

    public void setXauthKey(String key){
        headers.put("X-Auth-Key", key);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return value == null ? null : value.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    value, "utf-8");
            return null;
        }
    }

    @Override
    public String getBodyContentType() {
        return String.format("application/json; charset=utf-8");
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

}
