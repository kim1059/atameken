package kz.npp.nppapp.utils;

import java.util.ArrayList;
import java.util.List;

import kz.npp.nppapp.models.MapAddress;

/**
 * Created by kim1059 on 31.08.2016.
 */
public class Constants {

    public static String LOGIN = "login";
    public static String PASS = "pass";
    public static String FIRST_NAME = "fname";
    public static String LAST_NAME = "lname";
    public static String USER_ID = "userId";
    public static String USER_IIN = "iin";
    public static String MAIL = "email";
    public static String USER_DOC_ID = "docuemntId";
    public static boolean FROM_INFO = false;


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static List<MapAddress> MAP_ADDRESS_LIST = new ArrayList<>();
    public static boolean isNpp = true;
    /*API urls*/
    public static String API_URL = "http://services.palata.kz/";
    public static String XAUTHKEY = "rTFU-Ctl4qW4pHN";

    public  static String REGISTRATION_CONFIRM_URL = API_URL + "services/conf.php";
    public  static String LOGIN_URL = API_URL + "Synergy/rest/api/person/auth";
    public  static String SEARCH_URL = API_URL + "Synergy/rest/api/asforms/search/advanced";
    public  static String SIMPLE_SEARCH_URL = API_URL + "Synergy/rest/api/asforms/search";
    public  static String GET_DATA_URL = API_URL + "Synergy/rest/api/asforms/data/";
    public  static String SAVE_DATA_URL = API_URL + "Synergy/rest/api/asforms/data/save";
    public  static String CREATE_DOC_URL = API_URL + "Synergy/rest/api/registry/create_doc?registryID=";
    public  static String DOWNLOAD_IMAGE_URL = API_URL + "Synergy/rest/api/storage/file/get?identifier=";
    public  static String USER_INFO_URL = API_URL + "Synergy/rest/api/filecabinet/user/";
    public  static String START_UPLOAD_URL = API_URL + "Synergy/rest/api/storage/start_upload";
    public  static String DICTIONARY_URL = API_URL + "Synergy/rest/api/dictionary/get_by_code?dictionaryCode=";
    public  static String STORAGE_URL = API_URL + "Synergy/rest/api/storage/list";
    public  static String ADD_FILE_URL = API_URL + "Synergy/rest/api/storage/add";
    public  static String UPLOAD_PART_URL = API_URL + "Synergy/rest/api/storage/upload_part";
    public  static String PERSONAL_RECORD_URL = API_URL + "Synergy/rest/api/personalrecord/forms/";

    public  static String REGISTER_URL = API_URL + "npp/rest/user/registration";
    public  static String NEW_ORDER_URL = API_URL + "npp/rest/my_applications/create";
    public  static String KATO_URL = API_URL + "npp/rest/kato/";
    public  static String TRU_URL = API_URL + "npp/rest/tru/list?";
    public  static String MY_ORDERS_URL = API_URL + "npp/rest/my_applications/find/by_client?iin=";
    public  static String SEARCH_BIN_URL = API_URL + "npp/rest/contacts?bin=";
    public  static String SERVICE_NPP_URL = API_URL + "npp/rest/service/";
    public  static String SEND_TOKEN_URL = API_URL + "npp/rest/mobile/add";
    public  static String REMOVE_TOKEN_URL = API_URL + "npp/rest/mobile/remove";
    public  static String MY_APPLICATIONS_URL = API_URL + "npp/rest/my_applications/";
    public  static String RETRIEVE_LOGIN_URL = API_URL + "npp/rest/user/login?type=iin&login=";

    /*Form UUID*/
    public final static String RESULT_ID = "d36a2748-905e-4009-bf24-62211c2fb019";
    public final static String QUESTIONS_ID = "39f226e3-1ef1-48ea-a11e-4a2ec3875d9a";
    public final static String PROBLEMS_ID = "17bd2d18-e925-452d-a928-63a87ee022dc";
    public final static String LAWS_ID = "38b19c2d-8b60-43ef-9ce0-1fe24fca0a7a";
    public final static String DOCS_ID = "ed84dcce-5f4d-4d55-8d1f-7d4e54cfbf57";
    public final static String INTERNET_ID = "67c06d48-140f-4563-b82f-d47334789400";
    public final static String PLACES_ID = "111";
    public final static String ORDER_ID = "459699c1-4eb2-443a-8853-8a18aa902ebe";

    public final static String LAWS_INFO_ID = "5dc05852-9674-47fc-a75f-1739b508cc9b";
    public final static String INTERNET_INFO_ID = "755de136-90f1-4a7d-9454-f3abf052b708";
}
