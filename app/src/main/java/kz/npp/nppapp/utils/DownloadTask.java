package kz.npp.nppapp.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import kz.npp.nppapp.R;

/**
 * Created by kim1059 on 28.09.2016.
 */
public class DownloadTask extends AsyncTask<String, Integer, String> {

    private Context context;
    private ProgressDialog progressDialog;
    private SharedPreferences shared;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private File file;
    private String fileName;
    int id = 1;

    public DownloadTask(Context context) {
        this.context = context;
        shared = PreferenceManager.getDefaultSharedPreferences(context);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mNotifyManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("качивание файла")
                .setContentText("в процессе")
                .setSmallIcon(R.drawable.ic_download);

        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... params) {
        int count;
        try {
            URL url = new URL(params[0]);
            fileName = params[1];
            String creds = String.format("%s:%s",shared.getString(Constants.LOGIN,""),shared.getString(Constants.PASS,""));
            String auth = Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);

            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestProperty("Authorization", "Basic " + auth);
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.connect();
            int lenghtOfFile = httpUrlConnection.getContentLength();

            //String root = Environment.getExternalStorageDirectory().toString();
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            File myDir = new File(root);
            myDir.mkdirs();
            file = new File (myDir, fileName);

            // download the file
            InputStream input = httpUrlConnection.getInputStream();

            // Output stream
            OutputStream output = new FileOutputStream(file);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress((int)((total*100)/lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }

    /**
     * Updating progress bar
     * */
    protected void onProgressUpdate(Integer... progress) {
        // setting progress percentage
        progressDialog.setProgress(progress[0]);
        mBuilder.setProgress(100, progress[0], false);
        mNotifyManager.notify(id, mBuilder.build());
    }

    /**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String result) {
        // dismiss thog after the file was downloaded
        progressDialog.dismiss();

        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String extension = getFileExtension(fileName);
        String type = mime.getMimeTypeFromExtension(extension);

        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        //File file = new File("YOUR_SONG_URI"); // set your audio path
        intent.setDataAndType(Uri.fromFile(file), type);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        mBuilder.setContentText(fileName)
                // Removes the progress bar
                .setContentTitle("Скачивание завершено")
                .setContentIntent(pIntent)
                .setProgress(0,0,false);
        mNotifyManager.notify(id, mBuilder.build());

        if (result != null)
            Toast.makeText(context, "Ошибка: " + result, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, "Вы успешно скачали файл", Toast.LENGTH_SHORT).show();

    }

    private String getFileExtension(String name){
        String extension = "";

        int i = name.lastIndexOf('.');
        if (i > 0) {
            extension = name.substring(i+1);
        }
        return extension;
    }

}