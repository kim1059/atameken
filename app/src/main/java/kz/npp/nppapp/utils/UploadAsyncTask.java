package kz.npp.nppapp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import kz.npp.nppapp.fragments.MyFilesFragment;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * Created by kim1059 on 21.09.2016.
 */
public class UploadAsyncTask extends AsyncTask<String, Integer, String> {

    private Context context;
    private String user;
    private String password;
    private String fileName;
    private String serverTmpFile;
    private String filePath;
    private String parentId;
    private String dataToSend;
    private String fileId;
    private String docName;
    private String docCode;
    private SharedPreferences shared;
    private ProgressDialog progressDialog;

    private MyFilesFragment filesFragment;

    public UploadAsyncTask(Context context, MyFilesFragment filesFragment) {
        this.context = context;
        this.filesFragment = filesFragment;
        shared = PreferenceManager.getDefaultSharedPreferences(context);

    }

    @Override
    protected String doInBackground(String... params) {
        user = params[0];
        password = params[1];
        fileName = params[2];
        serverTmpFile = params[3];
        filePath = params[4];
        parentId = params[5];
        docCode = params[6];
        docName = params[7];

        FileInputStream inputStream = null;
        String boundary = "qweasdzxczxcasdqwe";
        String crlf = "\r\n";
        String twoHyphens = "--";

        try {
            int partSize = 1024*100;
            byte[] bytes = new byte[partSize];
            int position = 0;
            int read = 0;

            StringBuilder stringBuilder = new StringBuilder();

            URL url = new URL(Constants.UPLOAD_PART_URL + "?file=" + serverTmpFile);
            Log.e("url to ", url.toString());

            File file = new File(filePath);
            inputStream = new FileInputStream(file);

            while ((read = inputStream.read(bytes, 0, partSize)) != -1){

                byte [] readBytes = new byte[read];
                System.arraycopy(bytes, 0, readBytes, 0, read);
                position += read;
                Log.e("creds", String.valueOf(position));

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json; charset=utf-8");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Cache-Control", "no-cache");
                String creds = String.format("%s:%s",user,password);
                String auth = Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                conn.setRequestProperty("Authorization", "Basic "+auth);

                conn.setUseCaches (false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                DataOutputStream request = new DataOutputStream(conn.getOutputStream());
                request.writeBytes(twoHyphens + boundary + crlf);
                request.writeBytes("Content-Disposition: form-data; name=\"body\";filename=\"" + URLEncoder.encode(fileName, "utf-8")+ "\"" + crlf);
                request.writeBytes(crlf);

                byte[] fileBytes = Base64.encode(readBytes, Base64.NO_WRAP);
                request.write(fileBytes);
                request.writeBytes(crlf);
                request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);

                request.flush();
                request.close ();


                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                stringBuilder.append(response.toString());
                Log.e("result", response.toString());
            }
            Log.e("result", "end write bytes");

            String result = stringBuilder.toString();
            Log.e("result", result);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
       // progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
        if (progress.length == 2) {
            progressDialog.setProgress(progress[0]);
            progressDialog.setMax(progress[1]);
        }
    }

    @Override
    protected void onPostExecute(String result) {
       /* mWakeLock.release();*/
        PostUrlEncodedRequest fileRequest = postFileRequest(Constants.ADD_FILE_URL);
        fileRequest.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        fileRequest.setFileData(parentId, serverTmpFile, fileName);
        VolleySingleton.getInstance(context).addToRequestQueue(fileRequest);
    }

    private PostUrlEncodedRequest postFileRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            fileId = jsonObject.getString("fileID");
                            saveFileToRegister();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Не удалось загрузить файл", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("Profile", error.getMessage());
                    }
                });
    }

    public void saveFileToRegister() {
        JSONObject dateObject = new JSONObject();
        JSONObject userObject = new JSONObject();
        JSONObject fileObject = new JSONObject();
        JSONObject typeObject = new JSONObject();
        JSONArray dataArray = new JSONArray();

        try {
            dateObject.put("id", "date_time");
            dateObject.put("type", "date");
            dateObject.put("value", Utils.getJustDate());
            dateObject.put("key", Utils.getDateAndTime());

            userObject.put("id", "user");
            userObject.put("type", "entity");
            userObject.put("value", shared.getString(Constants.MAIL, ""));
            userObject.put("key", shared.getString(Constants.USER_ID, ""));

            fileObject.put("id", "link_to_file");
            fileObject.put("type", "filelink");
            fileObject.put("value", fileName);
            fileObject.put("key", fileId);

            typeObject.put("id", "type_doc");
            typeObject.put("type", "listbox");
            typeObject.put("value", docName);
            typeObject.put("key", docCode);

            dataArray.put(dateObject);
            dataArray.put(userObject);
            dataArray.put(fileObject);
            dataArray.put(typeObject);

            dataToSend = "\"data\":" + dataArray.toString();

            String registry;
            if (Constants.isNpp)
                registry = "dbe0fb24-6cb1-4d94-9ea9-b3b8f89b03f9";
            else
                registry = "b05f588d-3cd7-418d-aafa-cb0942b35e27";
            createDoc(registry);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void createDoc(String uuid) {
        GetRequest request = createDocRequest(Constants.CREATE_DOC_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(context).addToRequestQueue(request);
    }

    private void saveData(String uuid) {
        PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData("9c006d80-1576-4807-a471-297da20fcf64", uuid, dataToSend);
        VolleySingleton.getInstance(context).addToRequestQueue(request);
    }

    private GetRequest createDocRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String error = res.getString("errorCode");
                            if (!error.equalsIgnoreCase("0")){
                                progressDialog.dismiss();
                                Toast.makeText(context, "Не удалось создать пустую запись", Toast.LENGTH_LONG).show();
                                return;
                            }

                            saveData(res.getString("dataUUID"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        filesFragment.resumeFromUpload();
                        Toast.makeText(context, "файл сохранен", Toast.LENGTH_LONG).show();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Не удалось сохранить данные", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
