package kz.npp.nppapp.models;

import java.io.Serializable;

/**
 * Created by kim1059 on 28.09.2016.
 */
public class Dictionary implements Serializable {
    private String name;
    private String code;

    public Dictionary() {}

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }
}
