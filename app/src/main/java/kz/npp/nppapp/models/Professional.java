package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class Professional {
    private String name;
    private String avatar;

    public Professional(){}

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setName(String name) {
        this.name = name;
    }
}
