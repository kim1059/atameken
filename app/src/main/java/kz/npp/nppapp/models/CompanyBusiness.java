package kz.npp.nppapp.models;

import java.io.Serializable;

/**
 * Created by kim1059 on 19.09.2016.
 */
public class CompanyBusiness implements Serializable{
    private String date;
    private String orgType;
    private String oked;
    private String extraOked;
    private String region;
    private String raion;
    private String kato;
    private String okpo;
    private String krp;
    private String name;
    private String urAddress;
    private String factAddress;
    private String bin;
    private String lattitude;
    private String longitude;

    public CompanyBusiness(){}

    public String getDate() {
        return date;
    }

    public String getExtraOked() {
        return extraOked;
    }

    public String getKato() {
        return kato;
    }

    public String getKrp() {
        return krp;
    }

    public String getOked() {
        return oked;
    }

    public String getOkpo() {
        return okpo;
    }

    public String getOrgType() {
        return orgType;
    }

    public String getRaion() {
        return raion;
    }

    public String getRegion() {
        return region;
    }

    public String getName() {
        return name;
    }

    public String getFactAddress() {
        return factAddress;
    }

    public String getBin() {
        return bin;
    }

    public String getLattitude() {
        return lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExtraOked(String extraOked) {
        this.extraOked = extraOked;
    }

    public void setKato(String kato) {
        this.kato = kato;
    }

    public String getUrAddress() {
        return urAddress;
    }

    public void setKrp(String krp) {
        this.krp = krp;
    }

    public void setOked(String oked) {
        this.oked = oked;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setUrAddress(String urAddress) {
        this.urAddress = urAddress;
    }

    public void setFactAddress(String factAddress) {
        this.factAddress = factAddress;
    }
}
