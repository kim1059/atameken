package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 20.09.2016.
 */
public class Kato {
    private String name;
    private String code;
    private String displayName;

    public Kato(){}

    public String getDisplayName() {
        return displayName;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
