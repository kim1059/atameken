package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 14.10.2016.
 */
public class Opros {
    private String name;
    private String value;

    public Opros(){}

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
