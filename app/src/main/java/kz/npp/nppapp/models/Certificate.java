package kz.npp.nppapp.models;

import java.io.Serializable;

/**
 * Created by kim1059 on 30.09.2016.
 */
public class Certificate implements Serializable{
    private String id;
    private String num;
    private String date;
    private String tnved;
    private String kpved;
    private String mkei;
    private String dateExp;
    private String company;
    private String region;
    private String descr;

    public Certificate() {}

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public String getCompany() {
        return company;
    }

    public String getDateExp() {
        return dateExp;
    }

    public String getDescr() {
        return descr;
    }

    public String getKpved() {
        return kpved;
    }

    public String getNum() {
        return num;
    }

    public String getMkei() {
        return mkei;
    }

    public String getRegion() {
        return region;
    }

    public String getTnved() {
        return tnved;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setDateExp(String dateExp) {
        this.dateExp = dateExp;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public void setKpved(String kpved) {
        this.kpved = kpved;
    }

    public void setMkei(String mkei) {
        this.mkei = mkei;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setTnved(String tnved) {
        this.tnved = tnved;
    }
}
