package kz.npp.nppapp.models;

import java.io.Serializable;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class Service implements Serializable{
    private String id;
    private String name;
    private String shortDesription;
    private String longDescription;
    private String documentId;
    private String serviceNum;
    private String imageUrl;
    private String systemReceiver;
    private String formId;

    public Service() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public void setShortDesription(String shortDesription) {
        this.shortDesription = shortDesription;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public void setServiceNum(String serviceNum) {
        this.serviceNum = serviceNum;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setSystemReceiver(String systemReceiver) {
        this.systemReceiver = systemReceiver;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getShortDesription() {
        return shortDesription;
    }

    public String getDocumentId() {
        return documentId;
    }

    public String getServiceNum() {
        return serviceNum;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getSystemReceiver() {
        return systemReceiver;
    }

    public String getFormId() {
        return formId;
    }
}
