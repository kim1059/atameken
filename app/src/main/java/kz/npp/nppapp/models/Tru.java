package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 29.09.2016.
 */
public class Tru {
    private String id;
    private String name;
    private String label;
    private String description;

    public Tru() {}

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLabel() {
        return label;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setName(String name) {
        this.name = name;
    }
}
