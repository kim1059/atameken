package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 20.09.2016.
 */
public class Detalization {
    private String name;
    private String key;

    public Detalization(){}

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
