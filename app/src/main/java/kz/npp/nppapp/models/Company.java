package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 03.09.2016.
 */
public class Company {
    private String value;
    private String key;
    private String valueID;

    public Company(){}

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getValueID() {
        return valueID;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValueID(String valueID) {
        this.valueID = valueID;
    }
}
