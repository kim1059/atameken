package kz.npp.nppapp.models;

import java.io.Serializable;
import java.util.TreeMap;

/**
 * Created by kim1059 on 22.09.2016.
 */
public class Conversation implements Serializable {
    private String serviceName;
    private String uuid;
    private TreeMap<String, String> questions;
    private TreeMap<String, String> questionsDate;
    private TreeMap<String, String> answers;
    private TreeMap<String, String> answersDate;
    private String clientInfo;
    private String nameServiceInfo;
    private String statusInfo;

    public Conversation() {}

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setAnswers(TreeMap<String, String> answers) {
        this.answers = answers;
    }

    public void setAnswersDate(TreeMap<String, String> answersDate) {
        this.answersDate = answersDate;
    }

    public void setQuestions(TreeMap<String, String> questions) {
        this.questions = questions;
    }

    public void setQuestionsDate(TreeMap<String, String> questionsDate) {
        this.questionsDate = questionsDate;
    }

    public void setNameServiceInfo(String nameServiceInfo) {
        this.nameServiceInfo = nameServiceInfo;
    }

    public TreeMap<String, String> getAnswers() {
        return answers;
    }

    public TreeMap<String, String> getAnswersDate() {
        return answersDate;
    }

    public TreeMap<String, String> getQuestions() {
        return questions;
    }

    public TreeMap<String, String> getQuestionsDate() {
        return questionsDate;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getNameServiceInfo() {
        return nameServiceInfo;
    }

    public String getUuid() {
        return uuid;
    }
}
