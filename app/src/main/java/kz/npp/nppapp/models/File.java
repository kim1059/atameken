package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 28.09.2016.
 */
public class File {
    private String name;
    private String fileName;
    private String date;
    private String url;

    public File() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getUrl() {
        return url;
    }
}
