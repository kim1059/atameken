package kz.npp.nppapp.models;

import java.io.Serializable;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class Order implements Serializable{
    private String id;
    private String orderUuid;
    private String applicationNum;
    private String orderName;
    private String serviceName;
    private String serviceNum;
    private String status;
    private String date;
    private String executorId;
    private String executorWorkPhone;
    private String executorHomePhone;

    public Order(){}

    public String getId() {
        return id;
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    public String getApplicationNum() {
        return applicationNum;
    }

    public String getOrderName() {
        return orderName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getExecutorId() {
        return executorId;
    }

    public String getExecutorWorkPhone() {
        return executorWorkPhone;
    }

    public String getExecutorHomePhone() {
        return executorHomePhone;
    }

    public String getServiceNum() {
        return serviceNum;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOrderUuid(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public void setApplicationNum(String applicationNum) {
        this.applicationNum = applicationNum;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExecutorId(String executorId) {
        this.executorId = executorId;
    }

    public void setExecutorWorkPhone(String executorWorkPhone) {
        this.executorWorkPhone = executorWorkPhone;
    }

    public void setExecutorHomePhone(String executorHomePhone) {
        this.executorHomePhone = executorHomePhone;
    }

    public void setServiceNum(String serviceNum) {
        this.serviceNum = serviceNum;
    }
}
