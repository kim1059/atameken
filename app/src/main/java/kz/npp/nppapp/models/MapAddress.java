package kz.npp.nppapp.models;

/**
 * Created by kim1059 on 03.10.2016.
 */
public class MapAddress {
    private String address;
    private String phone;
    private String email;
    private String lattitude;
    private String longitude;

    private String mondayFrom;
    private String mondayTo;

    private String tuesdayFrom;
    private String tuesdayTo;

    private String wednesdayFrom;
    private String wednesdayTo;

    private String thursdayFrom;
    private String thursdayTo;

    private String fridayFrom;
    private String fridayTo;

    private String saturdayFrom;
    private String saturdayTo;

    private String sundayFrom;
    private String sundayTo;

    public MapAddress() {}

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getMondayFrom() {
        return mondayFrom;
    }

    public String getMondayTo() {
        return mondayTo;
    }

    public String getPhone() {
        return phone;
    }

    public String getTuesdayFrom() {
        return tuesdayFrom;
    }

    public String getTuesdayTo() {
        return tuesdayTo;
    }

    public String getWednesdayFrom() {
        return wednesdayFrom;
    }

    public String getFridayFrom() {
        return fridayFrom;
    }

    public String getWednesdayTo() {
        return wednesdayTo;
    }

    public String getThursdayFrom() {
        return thursdayFrom;
    }

    public String getThursdayTo() {
        return thursdayTo;
    }

    public String getFridayTo() {
        return fridayTo;
    }

    public String getSaturdayFrom() {
        return saturdayFrom;
    }

    public String getSaturdayTo() {
        return saturdayTo;
    }

    public String getSundayFrom() {
        return sundayFrom;
    }

    public String getSundayTo() {
        return sundayTo;
    }

    public String getLattitude() {
        return lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFridayFrom(String fridayFrom) {
        this.fridayFrom = fridayFrom;
    }

    public void setMondayFrom(String mondayFrom) {
        this.mondayFrom = mondayFrom;
    }

    public void setMondayTo(String mondayTo) {
        this.mondayTo = mondayTo;
    }

    public void setFridayTo(String fridayTo) {
        this.fridayTo = fridayTo;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setThursdayFrom(String thursdayFrom) {
        this.thursdayFrom = thursdayFrom;
    }

    public void setThursdayTo(String thursdayTo) {
        this.thursdayTo = thursdayTo;
    }

    public void setTuesdayFrom(String tuesdayFrom) {
        this.tuesdayFrom = tuesdayFrom;
    }

    public void setTuesdayTo(String tuesdayTo) {
        this.tuesdayTo = tuesdayTo;
    }

    public void setWednesdayFrom(String wednesdayFrom) {
        this.wednesdayFrom = wednesdayFrom;
    }

    public void setWednesdayTo(String wednesdayTo) {
        this.wednesdayTo = wednesdayTo;
    }

    public void setSaturdayFrom(String saturdayFrom) {
        this.saturdayFrom = saturdayFrom;
    }

    public void setSaturdayTo(String saturdayTo) {
        this.saturdayTo = saturdayTo;
    }

    public void setSundayFrom(String sundayFrom) {
        this.sundayFrom = sundayFrom;
    }

    public void setSundayTo(String sundayTo) {
        this.sundayTo = sundayTo;
    }
}
