package kz.npp.nppapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.gcm.RegistrationIntentService;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edt_login)
    EditText edtLogin;
    @BindView(R.id.edt_pass)
    EditText edtPass;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.cb_0)
    CheckBox cb0;
    @BindView(R.id.cb_1)
    CheckBox cb1;
    @BindView(R.id.cb_3)
    CheckBox cb3;
    @BindView(R.id.cb_4)
    CheckBox cb4;

    private String login, password;
    private String documentId;
    SharedPreferences shared;

    private GetRequest loginReq;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        shared =  PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (!TextUtils.isEmpty(shared.getString(Constants.USER_ID, ""))) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }

        edtLogin.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                edtLogin.setError(null);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

    private void registerGcm() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.e("success", "success");
                } else {
                    Log.e("error", "error");
                }
            }
        };

        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.e("not supported", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @OnClick(R.id.cb_0)
    public void setCb0() {
        cb1.setChecked(false);
        cb3.setChecked(false);
        cb4.setChecked(false);
    }

    @OnClick(R.id.cb_1)
    public void setCb1() {
        cb0.setChecked(false);
        cb3.setChecked(false);
        cb4.setChecked(false);
    }

    @OnClick(R.id.cb_3)
    public void setCb3() {
        cb1.setChecked(false);
        cb0.setChecked(false);
        cb4.setChecked(false);
    }

    @OnClick(R.id.cb_4)
    public void setCb4() {
        cb1.setChecked(false);
        cb0.setChecked(false);
        cb3.setChecked(false);
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        login = edtLogin.getText().toString();
        password = edtPass.getText().toString();

        Utils.hideSoftKeyboard(LoginActivity.this);

        // Reset errors.
        edtLogin.setError(null);
        edtPass.setError(null);

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(login)) {
            edtLogin.setError(getString(R.string.can_not_be_null));
            focusView = edtLogin;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            edtPass.setError(getString(R.string.can_not_be_null));
            focusView = edtPass;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress();
            retrieveLogin(login);
           /* Log.e("aaa", Constants.LOGIN_URL);
            Log.e("sss", Constants.API_URL);

            loginReq = loginRequest(Constants.LOGIN_URL);
            loginReq.setCredentials(login, password);
            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(loginReq);*/
        }
    }

    @OnClick(R.id.tv_registration)
    public void openRegistration() {
        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
        startActivity(intent);
    }

    private void retrieveLogin(String iin) {
        GetRequest request = retrieveLoginRequest(Constants.RETRIEVE_LOGIN_URL + iin);
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getUserInfo() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("user_id");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='8aba0940-175d-484e-849c-38fb8dabbeef' AND user_id LIKE '"+shared.getString(Constants.USER_ID, "")+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

    }

    private void getUserData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest retrieveLoginRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);
                            login = res.getString("object");
                            loginReq = loginRequest(Constants.LOGIN_URL);
                            loginReq.setCredentials(login, password);
                            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(loginReq);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                String json = new String(error.networkResponse.data);
                                Log.e("error login", json);
                                try {
                                    JSONObject res = new JSONObject(json);
                                    String code = res.getString("code");
                                    if (!code.equalsIgnoreCase("0")){
                                        Toast.makeText(getApplicationContext(), "Пользователь с таким ИИН не зарегистрирован", Toast.LENGTH_LONG).show();
                                        return;
                                    } else
                                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else
                                Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest loginRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);
                            SharedPreferences.Editor editor = shared.edit();

                            JSONObject res = new JSONObject(response);

                            editor.putString(Constants.LOGIN, login);
                            editor.putString(Constants.PASS, password);
                            editor.putString(Constants.FIRST_NAME, res.getString("firstname"));
                            editor.putString(Constants.LAST_NAME, res.getString("lastname"));
                            editor.putString(Constants.USER_ID, res.getString("userid"));
                            editor.putString(Constants.MAIL, res.getString("mail"));
                            editor.apply();

                            getUserInfo();
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        /*if (loginReq.getStatusCode() == 401) {
                            Toast.makeText(getApplicationContext(), "Неправильный e-mail или пароль ", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/

                        Toast.makeText(getApplicationContext(), "Неправильный ИИН или пароль ", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                documentId = jsonObject.getString("dataUUID");
                                getUserData(documentId);
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Отсутствует карточка клиента", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("iin")) {
                                    SharedPreferences.Editor editor = shared.edit();
                                    editor.putString(Constants.USER_IIN, object.getString("value"));
                                    editor.putString(Constants.USER_DOC_ID, documentId);
                                    editor.apply();
                                }
                            }

                            //do gcm registration
                            registerGcm();

                            /*go next window*/
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
        btnLogin.setVisibility(View.GONE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        btnLogin.setVisibility(View.VISIBLE);
    }
}
