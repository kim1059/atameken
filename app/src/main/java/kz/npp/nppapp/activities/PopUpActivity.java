package kz.npp.nppapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.MapAddress;
import kz.npp.nppapp.utils.Constants;

public class PopUpActivity extends AppCompatActivity {

    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_tel)
    TextView tvTel;
    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.tv_mon_from)
    TextView tvMonFrom;
    @BindView(R.id.tv_mon_to)
    TextView tvMonTo;
    @BindView(R.id.tv_tue_from)
    TextView tvTueFrom;
    @BindView(R.id.tv_tue_to)
    TextView tvTueTo;
    @BindView(R.id.tv_wed_from)
    TextView tvWedFrom;
    @BindView(R.id.tv_wed_to)
    TextView tvWedTo;
    @BindView(R.id.tv_thu_from)
    TextView tvThuFrom;
    @BindView(R.id.tv_thu_to)
    TextView tvThuTo;
    @BindView(R.id.tv_fri_from)
    TextView tvFriFrom;
    @BindView(R.id.tv_fri_to)
    TextView tvFriTo;
    @BindView(R.id.tv_sat_from)
    TextView tvSatFrom;
    @BindView(R.id.tv_sat_to)
    TextView tvSatTo;
    @BindView(R.id.tv_sun_from)
    TextView tvSunFrom;
    @BindView(R.id.tv_sun_to)
    TextView tvSunTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up);
        ButterKnife.bind(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));

        String index = getIntent().getStringExtra("index");
        MapAddress mapAddress = Constants.MAP_ADDRESS_LIST.get(Integer.parseInt(index));

        tvAddress.setText(mapAddress.getAddress());
        tvEmail.setText(mapAddress.getEmail());
        tvTel.setText(mapAddress.getPhone());

        tvMonFrom.setText(mapAddress.getMondayFrom());
        tvMonTo.setText(mapAddress.getMondayTo());
        tvTueFrom.setText(mapAddress.getTuesdayFrom());
        tvTueTo.setText(mapAddress.getTuesdayTo());
        tvWedFrom.setText(mapAddress.getWednesdayFrom());
        tvWedTo.setText(mapAddress.getWednesdayTo());
        tvThuFrom.setText(mapAddress.getThursdayFrom());
        tvThuTo.setText(mapAddress.getThursdayTo());
        tvFriFrom.setText(mapAddress.getFridayFrom());
        tvFriTo.setText(mapAddress.getFridayTo());
        tvSatFrom.setText(mapAddress.getSaturdayFrom());
        tvSatTo.setText(mapAddress.getSaturdayTo());
        tvSunFrom.setText(mapAddress.getSundayFrom());
        tvSunTo.setText(mapAddress.getSundayTo());
    }
}
