package kz.npp.nppapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ReportProblemActivity extends AppCompatActivity {

    @BindView(R.id.edt_problem)
    EditText edtProblem;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private String dataToSend;
    private SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);
        ButterKnife.bind(this);

        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Сообщить об ошибке");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_send)
    public void doSend() {
        if (edtProblem.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Введите текст для отправки", Toast.LENGTH_LONG).show();
            hideProgress();
            return;
        }

        showProgress();
        Utils.hideSoftKeyboard(ReportProblemActivity.this);

        String problemText = edtProblem.getText().toString();
        JSONObject whoWriteObject = new JSONObject();
        JSONObject problemObject = new JSONObject();
        JSONArray dataArray = new JSONArray();

        try {
            problemObject.put("id", "problem");
            problemObject.put("type", "textbox");
            problemObject.put("value", problemText);

            whoWriteObject.put("id", "who_write");
            whoWriteObject.put("type", "entity");
            whoWriteObject.put("value", shared.getString(Constants.FIRST_NAME,"")
                    + " " + shared.getString(Constants.LAST_NAME, ""));
            whoWriteObject.put("key", shared.getString(Constants.USER_ID, ""));

            dataArray.put(whoWriteObject);
            dataArray.put(problemObject);

            dataToSend = "\"data\":" + dataArray.toString();

            createDoc("00b4c05c-7562-4cc7-b168-a57242ec6605");
        } catch (JSONException e) {
            e.printStackTrace();
            hideProgress();
        }
    }


    private void createDoc(String uuid) {
        GetRequest request = createDocRequest(Constants.CREATE_DOC_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void saveData(String uuid) {
        PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData("7f497cff-230c-4f52-9c44-828f3302735a", uuid, dataToSend);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }


    private GetRequest createDocRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String error = res.getString("errorCode");
                            if (!error.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Не удалось создать пустую запись", Toast.LENGTH_LONG).show();
                                return;
                            }

                            saveData(res.getString("dataUUID"));
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Ваш запрос отправлен", Toast.LENGTH_LONG).show();
                        finish();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        btnSend.setVisibility(View.VISIBLE);
    }
}
