package kz.npp.nppapp.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.DownloadTask;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ServiceSectionInfo extends AppCompatActivity {

    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.llt_parent)
    LinearLayout lltParent;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    @BindView(R.id.btn_ask)
    Button btnAsk;

    private String formId;
    private String serviceName;
    private String serviceDocId;
    private boolean isProblem;

    private SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_section_info);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Информация");

        formId = getIntent().getStringExtra("id");
        serviceName = getIntent().getStringExtra("service_name");
        serviceDocId = getIntent().getStringExtra("service_doc_id");

        if (formId.equals(Constants.PLACES_ID))
            constructPlaces("111");
        else {
            showProgress();
            if (formId.equals(Constants.PROBLEMS_ID) ||
                    formId.equals(Constants.QUESTIONS_ID)) {
                getQuestionsInfo();
            } else
                getInfo();
        }

        if (formId.equals(Constants.PROBLEMS_ID)) {
            isProblem = true;
            btnAsk.setText("Сообщить о проблеме");
            btnAsk.setVisibility(View.VISIBLE);
        } else if (formId.equals(Constants.QUESTIONS_ID)){
            isProblem = false;
            btnAsk.setText("Задать вопрос");
            btnAsk.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_ask)
    public void ask() {
        Intent intent = new Intent(ServiceSectionInfo.this, AskQuestion.class);
        intent.putExtra("is_problem", isProblem);
        intent.putExtra("service_name", serviceName);
        intent.putExtra("service_doc_id", serviceDocId);
        startActivity(intent);
    }

    private void getInfo() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("name_service");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+formId+"' and name_service= '"+serviceName+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getQuestionsInfo() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("services");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+formId+"' and services like '%|"+serviceDocId+"|%'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getLawData(String uuid) {
        GetRequest request = getLawDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getInternetData(String uuid) {
        GetRequest request = getInternetDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void constructResult(String response) {
        tvQuestion.setText("Что я получу в результате?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            JSONArray dataInside = null;
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("table_detailing")){
                    dataInside = new JSONArray(object.getString("data"));
                }
            }

            if (dataInside != null) {
                for (int i=0;i<dataInside.length();i++) {
                    JSONObject object = dataInside.getJSONObject(i);
                    if (object.has("key")) {
                        final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_image, lltParent, false);
                        TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                        tvText.setText(object.getString("value"));
                        lltParent.addView(inflatedLayout);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void constructQuestions(String response) {
        tvQuestion.setText("Какие вопросы возможно решить?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("question")){
                    final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                    TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                    tvText.setText(object.getString("value"));
                    lltParent.addView(inflatedLayout);
                } else if (object.getString("id").equalsIgnoreCase("answer")){
                    final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                    TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                    tvText.setText(object.getString("value"));
                    lltParent.addView(inflatedLayout);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void constructProblems(String response) {
        tvQuestion.setText("Какие проблемы возможно решить?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("problem")){
                    final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                    TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                    tvText.setText(object.getString("value"));
                    lltParent.addView(inflatedLayout);
                } else if (object.getString("id").equalsIgnoreCase("answer")){
                    final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                    TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                    tvText.setText(object.getString("value"));
                    lltParent.addView(inflatedLayout);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void constructLaws(String response) {
        tvQuestion.setText("Какие сопутствующие законы полезны?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            JSONArray dataInside = null;
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("table_npa")){
                    dataInside = new JSONArray(object.getString("data"));
                }
            }

            if (dataInside != null) {
                for (int i=0;i<dataInside.length();i++) {
                    JSONObject object = dataInside.getJSONObject(i);
                    if (object.getString("id").contains("link_act")) {
                        getLaw(object.getString("value"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void constructDocs(String response) {
        tvQuestion.setText("Какие документы я смогу получить в результате?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            JSONArray dataInside = null;
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("table_docs")){
                    dataInside = new JSONArray(object.getString("data"));
                }
            }

            String indexName = "";
            if (dataInside != null) {
                for (int i=0;i<dataInside.length();i++) {
                    JSONObject object = dataInside.getJSONObject(i);
                    if (object.getString("id").contains("description_ru-") && object.has("value")) {
                        final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_file_btm_line, lltParent, false);
                        TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_name);
                        ImageView ivDownload = (ImageView) inflatedLayout.findViewById(R.id.iv_download);
                        tvText.setText(object.getString("value"));
                        lltParent.addView(inflatedLayout);

                        ivDownload.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getApplicationContext(), "Файл не прикреплен", Toast.LENGTH_LONG).show();
                            }
                        });

                        indexName = object.getString("id").substring(15);
                        for (int j=0; j<dataInside.length();j++) {
                            JSONObject object1 = dataInside.getJSONObject(j);
                            if (object1.getString("id").contains(indexName) &&
                                    object1.getString("type").equalsIgnoreCase("file") &&
                                    object1.has("key")) {

                                final String key = object1.getString("key");
                                final String name = object1.getString("value");
                                ivDownload.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        DownloadTask downloadTask = new DownloadTask(ServiceSectionInfo.this);
                                        downloadTask.execute(Constants.DOWNLOAD_IMAGE_URL + key,name);
                                    }
                                });
                                break;

                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void constructInternet(String response) {
        tvQuestion.setText("Что полезно в интернете?");
        try {
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            JSONArray dataInside = null;
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("links_table")){
                    dataInside = new JSONArray(object.getString("data"));
                }
            }

            if (dataInside != null) {
                for (int i=0;i<dataInside.length();i++) {
                    JSONObject object = dataInside.getJSONObject(i);
                    if (object.getString("id").contains("internet_resource")) {
                        getInternet(object.getString("value"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void constructPlaces(String response) {
        tvQuestion.setText("Где я могу получить услугу?");
        View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_image, lltParent, false);
        TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Адрес 1");

        inflatedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceSectionInfo.this, MapActivity.class);
                startActivity(intent);
            }
        });
        Utils.makeTextViewHyperlink(tvText);
        lltParent.addView(inflatedLayout);
    }

    private void constructOrder(String response) {
        tvQuestion.setText("Какой порядок получения услуги?");
        try {
            Log.e("response", response);
            JSONObject res = new JSONObject(response);
            JSONArray data = new JSONArray(res.getString("data"));
            JSONArray dataInside = null;
            for (int i=0; i<data.length(); i++) {
                JSONObject object = data.getJSONObject(i);
                if (object.getString("id").equalsIgnoreCase("stage_table")){
                    dataInside = new JSONArray(object.getString("data"));
                }
            }

            if (dataInside != null) {
                for (int i=0;i<dataInside.length();i++) {
                    JSONObject object = dataInside.getJSONObject(i);
                    if (object.getString("id").contains("stage") && object.has("value")) {
                        final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_image, lltParent, false);
                        TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                        tvText.setText(object.getString("value"));
                        lltParent.addView(inflatedLayout);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getLaw(String name) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("name_doc");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+Constants.LAWS_INFO_ID+"' and name_doc= '"+name+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchLawRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getInternet(String name) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("resurs");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+Constants.INTERNET_INFO_ID+"' and resurs= '"+name+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchInternetRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getData(jsonObject.getString("dataUUID"));
                            }

                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Ничего нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest searchLawRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getLawData(jsonObject.getString("dataUUID"));
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest searchInternetRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getInternetData(jsonObject.getString("dataUUID"));
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }


    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();

                        if (formId.equals(Constants.RESULT_ID))
                            constructResult(response);
                        else if (formId.equals(Constants.QUESTIONS_ID))
                            constructQuestions(response);
                        else if (formId.equals(Constants.PROBLEMS_ID))
                            constructProblems(response);
                        else if (formId.equals(Constants.LAWS_ID))
                            constructLaws(response);
                        else if (formId.equals(Constants.DOCS_ID))
                            constructDocs(response);
                        else if (formId.equals(Constants.INTERNET_ID))
                            constructInternet(response);
                        else if (formId.equals(Constants.PLACES_ID))
                            constructPlaces(response);
                        else if (formId.equals(Constants.ORDER_ID))
                            constructOrder(response);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getLawDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            String link = "";
                            String nameDoc = "";
                            for (int i=0; i<data.length(); i++) {
                                final JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("law_link")){
                                    link = object.getString("value");
                                } else if (object.getString("id").equalsIgnoreCase("name_doc")) {
                                    nameDoc = object.getString("value");
                                }
                            }
                            final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                            TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                            tvText.setText(nameDoc);
                            Utils.makeTextViewHyperlink(tvText);

                            final String finalLink = link;
                            inflatedLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse(finalLink));
                                    startActivity(browserIntent);
                                }
                            });

                            lltParent.addView(inflatedLayout);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getInternetDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            String link = "";
                            String description = "";
                            for (int i=0; i<data.length(); i++) {
                                final JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("resurs")){
                                    link = object.getString("value");
                                } else if (object.getString("id").equalsIgnoreCase("description")) {
                                    description = object.getString("value");
                                }
                            }

                            final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                            TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                            tvText.setText(description);
                            Utils.makeTextViewHyperlink(tvText);

                            final String finalLink = getCorrectLink(link);
                            inflatedLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse(finalLink));
                                    startActivity(browserIntent);
                                }
                            });

                            lltParent.addView(inflatedLayout);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getApplicationContext(), "Ссылка не может быть открыта", Toast.LENGTH_LONG).show();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }


    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

    private String getCorrectLink(String url) {
        if(!url.startsWith("www.") && !url.startsWith("http://")){
            url = "www."+url;
        }
        if(!url.startsWith("http://")){
            url = "http://"+url;
        }

        return url;
    }
}
