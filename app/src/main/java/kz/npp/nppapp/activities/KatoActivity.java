package kz.npp.nppapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.KatoAdapter;
import kz.npp.nppapp.models.Kato;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class KatoActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Kato> adapter;
    private List<Kato> katoList = new ArrayList<>();
    private SharedPreferences shared;

    private final String ROOT_LEVEL = "root";

    private String level;
    private final int CHOOSE_KATO_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kato);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("КАТО");

        level = getIntent().getStringExtra("level");
        showProgress();
        /*get list of services*/
        getKato(getIntent().getStringExtra("code"));

        adapter = new KatoAdapter(getApplicationContext(), R.layout.activity_kato,katoList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (katoList.get(position).getDisplayName().equalsIgnoreCase("Все")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("name", katoList.get(position).getName());
                    returnIntent.putExtra("code", katoList.get(position).getCode());
                    returnIntent.putExtra("level", level);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Intent intent = new Intent(KatoActivity.this, KatoActivity.class);
                    intent.putExtra("name", katoList.get(position).getName());
                    intent.putExtra("code", katoList.get(position).getCode());
                    intent.putExtra("level", "not_root");
                    startActivityForResult(intent, CHOOSE_KATO_CODE);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_KATO_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("name", data.getStringExtra("name"));
                returnIntent.putExtra("code", data.getStringExtra("code"));
                returnIntent.putExtra("level", data.getStringExtra("level"));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    private void getKato(String code) {
        String nextLevel;
        if (level.equalsIgnoreCase(ROOT_LEVEL))
            nextLevel = "regions";
        else {
            nextLevel = code + "/child";
        }
        GetRequest request = getKatoRequest(Constants.KATO_URL + nextLevel);
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getKatoRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            Kato kato = new Kato();
                            if (level.equalsIgnoreCase(ROOT_LEVEL)) {
                                kato.setCode("0");
                                kato.setDisplayName("Все");
                                kato.setName("Все");
                            } else {
                                kato.setName(getIntent().getStringExtra("name"));
                                kato.setDisplayName("Все");
                                kato.setCode(trimEnd(getIntent().getStringExtra("code"), '0'));
                            }
                            katoList.add(kato);

                            for (int i=0;i<data.length();i++) {
                                JSONObject object = data.getJSONObject(i);
                                kato = new Kato();
                                kato.setName(object.getString("name"));
                                kato.setDisplayName(object.getString("name"));
                                kato.setCode(object.getString("code"));
                                katoList.add(kato);
                            }

                            if (katoList.size() == 1) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("name", getIntent().getStringExtra("name"));
                                returnIntent.putExtra("code", getIntent().getStringExtra("code"));
                                returnIntent.putExtra("level", level);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                                return;
                            }
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

    public static String trimEnd(String text, char character) {
        String normalizedText;
        int index;

        normalizedText = text.trim();
        index = normalizedText.length() - 1;

        while (normalizedText.charAt(index) == character) {
            if (--index < 0) {
                return "";
            }
        }
        Log.e("result", normalizedText.substring(0, index + 1).trim() + "%");
        return normalizedText.substring(0, index + 1).trim() + "%";
    }

}
