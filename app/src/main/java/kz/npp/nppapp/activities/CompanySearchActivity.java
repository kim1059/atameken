package kz.npp.nppapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;

public class CompanySearchActivity extends AppCompatActivity {

    @BindView(R.id.edt_company_name)
    EditText edtCompanyName;
    @BindView(R.id.edt_kato)
    EditText edtKato;
    @BindView(R.id.edt_bin)
    EditText edtBin;
    @BindView(R.id.edt_tru)
    EditText edtTru;
    @BindView(R.id.sw_certificate)
    Switch swCertificate;

    private final int CHOOSE_KATO_CODE = 100;
    private final int CHOOSE_TRU_CODE = 200;
    private String katoName;
    private String katoCode;

    private String truName;
    private String truDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_search);
        ButterKnife.bind(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Поиск компаний");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_KATO_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                katoName = data.getStringExtra("name");
                katoCode = data.getStringExtra("code");
                edtKato.setText(katoName);

                String level = data.getStringExtra("level");
                if (katoCode.equalsIgnoreCase("0"))
                    katoCode = "";
                if (level.equalsIgnoreCase("first_level")) {
                    katoCode = katoCode.substring(0, 2) + "%";
                } else if (level.equalsIgnoreCase("second_level")) {
                    katoCode = katoCode.substring(0, 4) + "%";
                }
            }
        } else if (requestCode == CHOOSE_TRU_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                truName = data.getStringExtra("name");
                truDesc = data.getStringExtra("descr");
                edtTru.setText(truName);
            }
        }
    }


    @OnClick(R.id.edt_kato)
    public void chooseKato() {
        Intent intent = new Intent(CompanySearchActivity.this, KatoActivity.class);
        intent.putExtra("level", "root");
        startActivityForResult(intent, CHOOSE_KATO_CODE);
    }

    @OnClick(R.id.edt_tru)
    public void chooseTru() {
        Intent intent = new Intent(CompanySearchActivity.this, TruActivity.class);
        intent.putExtra("parent", "");
        intent.putExtra("start", 0);
        startActivityForResult(intent, CHOOSE_TRU_CODE);
    }

    @OnClick(R.id.btn_search)
    public void doSearch() {
        Intent intent = new Intent(CompanySearchActivity.this, CompaniesResultListActivity.class);
        intent.putExtra("certificate", swCertificate.isChecked());
        intent.putExtra("bin", edtBin.getText().toString());
        intent.putExtra("kato", katoCode);
        intent.putExtra("name", edtCompanyName.getText().toString());
        intent.putExtra("tru", edtTru.getText().toString());
        startActivity(intent);
    }
}
