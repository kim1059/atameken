package kz.npp.nppapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.CompaniesAdapter;
import kz.npp.nppapp.models.Company;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ChooseCompanyActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Company> adapter;
    private List<Company> companiesList = new ArrayList<>();
    private SharedPreferences shared;

    private String documentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_company);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Компании");

        showProgress();
        /*get list of services*/
        getCompanies();

        adapter = new CompaniesAdapter(getApplicationContext(), R.layout.activity_choose_company,companiesList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("value", companiesList.get(position).getValue());
                returnIntent.putExtra("value_id", companiesList.get(position).getValueID());
                returnIntent.putExtra("key", companiesList.get(position).getKey());
                returnIntent.putExtra("document_id", documentId);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getCompanies() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("iin");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='8aba0940-175d-484e-849c-38fb8dabbeef' and iin= '"+shared.getString(Constants.USER_IIN, "")+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getData(jsonObject.getString("dataUUID"));
                                documentId = jsonObject.getString("documentID");
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Компаний пока нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0;i<data.length();i++) {
                                JSONObject companyObject = data.getJSONObject(i);
                                if (companyObject.getString("id").equalsIgnoreCase("t3")) {
                                    JSONArray companyArray = new JSONArray(companyObject.getString("data"));
                                    for (int j = 0; j < companyArray.length(); j++) {
                                        JSONObject object = companyArray.getJSONObject(j);
                                        Company company = new Company();
                                        company.setValue(object.getString("value"));
                                        company.setKey(object.getString("key"));
                                        company.setValueID(object.getString("valueID"));
                                        companiesList.add(company);
                                    }
                                }
                            }

                            if (companiesList.size() == 0)
                                Toast.makeText(getApplicationContext(), "Компаний пока нет", Toast.LENGTH_LONG).show();

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }
}
