package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Dictionary;
import kz.npp.nppapp.models.Order;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.DownloadTask;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class MyOrderDetailsActivity extends AppCompatActivity {

    @BindView(R.id.tv_service_name)
    TextView tvServiceName;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_executor_name)
    TextView tvExecutorName;
    @BindView(R.id.tv_position)
    TextView tvPosition;
    @BindView(R.id.llt_docs)
    LinearLayout lltDocs;
    @BindView(R.id.btn_opros)
    Button btnOpros;

    private TextView tvTitle;
    private SharedPreferences shared;
    private Order order;
    private HashMap<String, String> statusMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_details);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (getIntent().getBooleanExtra("from_push", false)) {
            getDictionary();
        } else {
            order = (Order) getIntent().getSerializableExtra("order");
            setOrderData();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra("from_push", false)) {
            Intent intent = new Intent(MyOrderDetailsActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.iv_call)
    public void doCall() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + order.getExecutorHomePhone()));
        startActivity(callIntent);
    }

    @OnClick(R.id.btn_ask)
    public void ask() {
        Intent intent = new Intent(MyOrderDetailsActivity.this, AskQuestion.class);
        intent.putExtra("is_problem", false);
        intent.putExtra("is_order", true);
        intent.putExtra("service_name", order.getServiceName());
        //intent.putExtra("service_doc_id", serviceDocId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_opros)
    public void opros() {
        Intent intent = new Intent(MyOrderDetailsActivity.this, OprosListActivity.class);
        intent.putExtra("order_uuid", order.getOrderUuid());
        //intent.putExtra("service_num", order.getServiceNum());
        intent.putExtra("service_name", order.getServiceName());
        startActivity(intent);
    }

    private void setOrderData() {
        if (order.getStatus().equalsIgnoreCase("Исполнена")) {
            btnOpros.setVisibility(View.VISIBLE);
        }
        tvTitle.setText(order.getOrderName());
        tvServiceName.setText(order.getServiceName());
        tvDate.setText(order.getDate());
        tvStatus.setText(order.getStatus());

        getExecutorPhone();
        getExecutorInfo();
        getDocs();
    }

    private void getDictionary() {
        GetRequest request = getDictionaryRequest(Constants.DICTIONARY_URL + "status_anketa");
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getExecutorInfo() {
        if (order.getExecutorId() == null)
            return;
        GetRequest request = getDataRequest(Constants.USER_INFO_URL + order.getExecutorId());
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getExecutorPhone() {
        GetRequest request = getExecutorPhoneRequest(Constants.PERSONAL_RECORD_URL + order.getExecutorId());
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getPhoneData(String uuid) {
        GetRequest request = getPhoneDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

   /* private void getOrderInfo(String orderId) {
        GetRequest request = getOrderInfoRequest(Constants.MY_APPLICATIONS_URL + orderId);
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }*/

    private void getOrderInfo(String orderId) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("application_id");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='582ce05d-5b10-4639-bd0b-865993061b86' " +
                    "AND application_id LIKE '"+orderId+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Log.e("allinfo myorder",allInfo.toString());
        PostRequest request = myOrdersRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getOrderData(String uuid) {
        GetRequest request = getOrderDataRequest(Constants.GET_DATA_URL + uuid, uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getDocs() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("application_id");
        String url;
        if (Constants.isNpp) {
            url = "3cbc2d47-e883-4cb4-a9d3-ce689654aea1";
        } else {
            url = "3cbc2d47-e883-4cb4-a9d3-ce689654aea1";
        }
        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+url+"' AND application_id='"+order.getId()+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getDocsData(String uuid) {
        GetRequest request = getDocsDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest myOrdersRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);

                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getOrderData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                Toast.makeText(getApplicationContext(), "Заявки нет", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getOrderDataRequest(final String url, final String uuid) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response getOrderData", response);
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            order = new Order();
                            order.setOrderUuid(uuid);
                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("date")) {
                                    order.setDate(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("application_id")) {
                                    order.setId(object.getString("value"));
                                } else if (object.getString("id").equalsIgnoreCase("num_zayavka")) {
                                    order.setApplicationNum(object.getString("value"));
                                    order.setOrderName("Заявка " + order.getApplicationNum());
                                } else if (object.getString("id").equalsIgnoreCase("executor")) {
                                    order.setExecutorId(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("name_service")) {
                                    order.setServiceName(object.getString("value"));
                                } else if (object.getString("id").equalsIgnoreCase("status_anketa")) {
                                    order.setStatus(statusMap.get(object.getString("key")));
                                }
                            }
                            setOrderData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getDocsData(jsonObject.getString("dataUUID"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getOrderInfoRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);
                            JSONObject object = new JSONObject(res.getString("object"));

                            order = new Order();
                            String date = getDate(object.getLong("dateStartService"));
                            order.setId(object.getString("id"));
                            if (object.getString("applicationNumber").equalsIgnoreCase("null"))
                                order.setApplicationNum("(без номера)");
                            else
                                order.setApplicationNum(object.getString("applicationNumber"));
                            order.setDate(date);
                            order.setServiceName(object.getString("serviceName"));
                            order.setServiceNum(object.getString("serviceNumber"));
                            order.setOrderName("Заявка " + order.getApplicationNum());
                            order.setExecutorId(object.getString("executorId"));
                            order.setStatus(statusMap.get(object.getString("status")));
                            order.setExecutorHomePhone(
                                    object.getString("executorHomePhone").equalsIgnoreCase("null") ? "" : object.getString("executorHomePhone"));

                            setOrderData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplication(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDictionaryRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(res.getString("items"));
                            statusMap = new HashMap<String, String>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONArray valuesArray = new JSONArray(jsonObject.getString("values"));
                                Dictionary dictionary = new Dictionary();

                                JSONObject valueObject1 = valuesArray.getJSONObject(0);
                                JSONObject valueObject2 = valuesArray.getJSONObject(1);
                                if (valueObject1.getString("value").length() > 1) {
                                    dictionary.setName(valueObject1.getString("value"));
                                    dictionary.setCode(valueObject2.getString("value"));
                                    statusMap.put(valueObject2.getString("value"), valueObject1.getString("value"));
                                } else {
                                    dictionary.setCode(valueObject1.getString("value"));
                                    dictionary.setName(valueObject2.getString("value"));
                                    statusMap.put(valueObject1.getString("value"), valueObject2.getString("value"));
                                }

                            }

                            getOrderInfo(getIntent().getStringExtra("order_id"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDocsDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            String indexName = "";
                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("table_docs")) {
                                    JSONArray jsonArray = new JSONArray(object.getString("data"));
                                    for (int j=0;j<jsonArray.length();j++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                                        /*if (jsonObject.getString("id").contains("description_ru")) {
                                            final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_doc, lltDocs, false);
                                            TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_name);
                                            tvText.setText(jsonObject.getString("value"));
                                            lltDocs.addView(inflatedLayout);
                                        }*/
                                        if (jsonObject.getString("id").contains("description_ru-") && jsonObject.has("value")) {
                                            final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_file_btm_line, lltDocs, false);
                                            TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_name);
                                            ImageView ivDownload = (ImageView) inflatedLayout.findViewById(R.id.iv_download);
                                            tvText.setText(jsonObject.getString("value"));
                                            lltDocs.addView(inflatedLayout);

                                            ivDownload.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Toast.makeText(getApplicationContext(), "Файл не прикреплен", Toast.LENGTH_LONG).show();
                                                }
                                            });

                                            indexName = jsonObject.getString("id").substring(15);
                                            for (int k=0; k<jsonArray.length();k++) {
                                                JSONObject object1 = jsonArray.getJSONObject(k);
                                                if (object1.getString("id").contains(indexName) &&
                                                        object1.getString("type").equalsIgnoreCase("file") &&
                                                        object1.has("key")) {

                                                    final String key = object1.getString("key");
                                                    final String name = object1.getString("value");
                                                    ivDownload.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            DownloadTask downloadTask = new DownloadTask(MyOrderDetailsActivity.this);
                                                            downloadTask.execute(Constants.DOWNLOAD_IMAGE_URL + key,name);
                                                        }
                                                    });
                                                    break;

                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            String name = res.getString("lastname") +" "+ res.getString("firstname");
                            tvExecutorName.setText(name);

                            JSONArray jsonArray = new JSONArray(res.getString("positions"));
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                tvPosition.setText(jsonObject.getString("positionName"));
                                break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getExecutorPhoneRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject res = jsonArray.getJSONObject(i);
                                getPhoneData(res.getString("data-uuid"));
                                break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getPhoneDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("phone_number_2")) {
                                    order.setExecutorHomePhone(object.getString("value"));
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy hh:mm", cal).toString();
        return date;
    }

}
