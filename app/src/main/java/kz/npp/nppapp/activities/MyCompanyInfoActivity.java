package kz.npp.nppapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class MyCompanyInfoActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_bin)
    TextView tvBin;
    @BindView(R.id.tv_kato)
    TextView tvKato;
    @BindView(R.id.tv_ur_address)
    TextView tvUrAddress;
    @BindView(R.id.tv_registration)
    TextView tvRegistration;

    private SharedPreferences shared;
    private String name;
    private String documentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_company_info);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Компания");

        name = getIntent().getStringExtra("value");
        documentId = getIntent().getStringExtra("value_id");

        getCompanyInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getCompanyInfo() {
        String query = "where uuid='297fcb61-3bc1-4b80-965f-b8ddfccf1270' AND documentID LIKE '"+documentId+"'";
        ArrayList<String> list = new ArrayList<String>();

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", query);
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getCompanyData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getCompanyData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                Toast.makeText(getApplicationContext(), "Не правильный documentID", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject date = null;
                            JSONObject bin = null;
                            JSONObject orgType = null;
                            JSONObject oked = null;
                            JSONObject extraOked = null;
                            JSONObject region = null;
                            JSONObject raion = null;
                            JSONObject kato = null;
                            JSONObject okpo = null;
                            JSONObject krp = null;
                            JSONObject name = null;
                            JSONObject urAddress = null;
                            JSONObject factAddress = null;

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("organization_type")) {
                                    orgType = object;
                                } else if (object.getString("id").equals("organization_name")) {
                                    name = object;
                                }  else if (object.getString("id").equals("iin_bin")) {
                                    bin = object;
                                } else if (object.getString("id").equals("kato")) {
                                    kato = object;
                                } else if (object.getString("id").equals("ur_adres")) {
                                    urAddress = object;
                                } else if (object.getString("id").equals("oked")) {
                                    oked = object;
                                } else if (object.getString("id").equals("vtor_oked")) {
                                    extraOked = object;
                                } else if (object.getString("id").equals("region")) {
                                    region = object;
                                } else if (object.getString("id").equals("rayon")) {
                                    raion = object;
                                } else if (object.getString("id").equals("krp_name")) {
                                    krp = object;
                                } else if (object.getString("id").equals("okpo")) {
                                    okpo = object;
                                } else if (object.getString("id").equals("registr_date")) {
                                    date = object;
                                } else if (object.getString("id").equals("t")) {
                                    JSONArray dataAddress = new JSONArray(object.getString("data"));
                                    for (int j=0; j<dataAddress.length(); j++) {
                                        JSONObject addressObject = dataAddress.getJSONObject(j);
                                        if (addressObject.getString("id").contains("fakt_adres")) {
                                            factAddress = addressObject;
                                        }
                                    }
                                }
                            }

                            if (name != null) {
                                tvName.setText(name.getString("value"));
                            }
                            if (kato != null) {
                                tvKato.setText(kato.getString("value"));
                            }
                            if (urAddress != null) {
                                tvUrAddress.setText(urAddress.getString("value"));
                            }
                            if (date != null) {
                                tvRegistration.setText(date.getString("key"));
                            }
                            if (bin != null) {
                                tvBin.setText(bin.getString("value"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }
}
