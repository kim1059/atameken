package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.CompanyBusiness;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class CompanySectionInfoActivity extends AppCompatActivity {

    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.llt_parent)
    LinearLayout lltParent;
    @BindView(R.id.tv_topic)
    TextView tvTopic;

    private SharedPreferences shared;
    private String formId;
    private CompanyBusiness company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_section_info);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Информация");

        formId = getIntent().getStringExtra("id");
        company = (CompanyBusiness) getIntent().getSerializableExtra("company");

        getInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getInfo() {
        if (formId.equalsIgnoreCase("general_info")) {
            constructGeneralInfo();
        } else if (formId.equalsIgnoreCase("address")) {
            constructAddress();
        } else if (formId.equalsIgnoreCase("certificates")) {
            constructCertificates();
        } else if (formId.equalsIgnoreCase("telephones")) {
            constructTelephones();
        }
    }

    private void constructGeneralInfo() {
        tvTopic.setText("Общие сведения");
        View inflatedLayout;
        TextView tvText;

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Дата регистрации");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getDate());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Организационно правовая форма");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getOrgType());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Код ОКЭД");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getOked());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Вторичный код ОКЭД");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getExtraOked());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Регион");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getRegion());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Район");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getRaion());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Код КАТО");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getKato());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Код ОКПО");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getOkpo());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Код КРП");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getKrp());
        lltParent.addView(inflatedLayout);

    }

    private void constructAddress() {
        tvTopic.setText("Адреса");
        View inflatedLayout;
        TextView tvText;

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Юридический адрес");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getUrAddress());
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText("Фактический адрес ");
        lltParent.addView(inflatedLayout);

        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
        tvText.setText(company.getFactAddress());
        lltParent.addView(inflatedLayout);

    }

    private void constructCertificates() {
        tvTopic.setText("Сертификаты");
    }

    private void constructTelephones() {
        tvTopic.setText("Контакты");
        getPhones();
    }

    private void getPhones() {
        GetRequest request = getContactsRequest(Constants.SEARCH_BIN_URL + company.getBin());
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getContactsRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            View inflatedLayout;
                            TextView tvText;
                            JSONArray res = new JSONArray(response);

                            boolean exists = false;
                            for (int i=0;i<res.length();i++) {
                                JSONObject object = res.getJSONObject(i);
                                JSONArray phones = new JSONArray(object.getString("phones"));
                                JSONArray emails = new JSONArray(object.getString("emails"));
                                String name = object.getString("fio");
                                String position = object.getString("position");

                                if (!TextUtils.isEmpty(name) && !name.equalsIgnoreCase("null")){
                                    inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                                    tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                    tvText.setText("Контактное лицо");
                                    lltParent.addView(inflatedLayout);

                                    inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                                    tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                    tvText.setText(name);
                                    lltParent.addView(inflatedLayout);
                                }

                                if (!TextUtils.isEmpty(position) && !position.equalsIgnoreCase("null")){
                                    inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                                    tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                    tvText.setText("Должность");
                                    lltParent.addView(inflatedLayout);

                                    inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                                    tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                    tvText.setText(position);
                                    lltParent.addView(inflatedLayout);
                                }

                                for (int j=0; j<phones.length();j++) {
                                    JSONObject phone = phones.getJSONObject(j);
                                    final String phoneNum = phone.getString("phone");

                                    if (!TextUtils.isEmpty(phoneNum) && !phoneNum.equalsIgnoreCase("null")){
                                        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                                        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                        tvText.setText("Телефон");
                                        lltParent.addView(inflatedLayout);

                                        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                                        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                        tvText.setText(phoneNum);
                                        Utils.makeTextViewHyperlink(tvText);
                                        lltParent.addView(inflatedLayout);

                                        inflatedLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                doCall(phoneNum.replaceAll("\\D", ""));
                                            }
                                        });

                                        exists = true;
                                    }
                                }

                                for (int j=0; j<emails.length();j++) {
                                    JSONObject email = emails.getJSONObject(j);
                                    final String emailTxt = email.getString("email");

                                    if (!TextUtils.isEmpty(emailTxt) && !emailTxt.equalsIgnoreCase("null")){
                                        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_bold, lltParent, false);
                                        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                        tvText.setText("Email");
                                        lltParent.addView(inflatedLayout);

                                        inflatedLayout = getLayoutInflater().inflate(R.layout.row_text_raw, lltParent, false);
                                        tvText = (TextView) inflatedLayout.findViewById(R.id.tv_text);
                                        tvText.setText(emailTxt);
                                        Utils.makeTextViewHyperlink(tvText);
                                        lltParent.addView(inflatedLayout);

                                        inflatedLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                doEmail(emailTxt);
                                            }
                                        });
                                        exists = true;
                                    }
                                }
                            }

                            if (!exists) {
                                Toast.makeText(getApplicationContext(), "Контакты отсутствуют", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void doCall(String tel) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:+7" +tel));
        startActivity(callIntent);
    }

    private void doEmail (String email) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Тема");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "текст");

        startActivity(Intent.createChooser(emailIntent, "Отправить email..."));
    }
}
