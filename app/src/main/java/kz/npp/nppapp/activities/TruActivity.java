package kz.npp.nppapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.TruAdapter;
import kz.npp.nppapp.models.Tru;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class TruActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private View footerView;

    private ArrayAdapter<Tru> adapter;
    private List<Tru> truList = new ArrayList<>();
    private SharedPreferences shared;

    private String parentId;
    private int start;
    private String nextItemsUrl;
    private boolean listLoading = false;
    private boolean isEnd = false;

    private final int CHOOSE_TRU_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tru);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("ТРУ");

        parentId = getIntent().getStringExtra("parent");
        start = getIntent().getIntExtra("start", 0);

        showProgress();
        /*get list of services*/
        getTru(parentId, start);

        adapter = new TruAdapter(getApplicationContext(), R.layout.activity_tru,truList);
        footerView = ((LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer_progress, lvList, false);
        lvList.addFooterView(footerView);
        lvList.setAdapter(adapter);
        lvList.removeFooterView(footerView);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TruActivity.this, TruActivity.class);
                intent.putExtra("name", truList.get(position).getName());
                intent.putExtra("descr", truList.get(position).getDescription());
                if (!TextUtils.isEmpty(parentId))
                    intent.putExtra("parent", parentId + "." + truList.get(position).getId());
                else
                    intent.putExtra("parent", truList.get(position).getId());
                intent.putExtra("start", 0);
                startActivityForResult(intent, CHOOSE_TRU_CODE);
            }
        });

        lvList.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount > 29) {
                    if (!listLoading && !isEnd) {
                        listLoading = true;
                        lvList.addFooterView(footerView);

                        getTru(parentId, start);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_TRU_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("name", data.getStringExtra("name"));
                returnIntent.putExtra("descr", data.getStringExtra("descr"));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    private void getTru(String parent, int start) {
        nextItemsUrl = "start=" + String.valueOf(start);
        if (!TextUtils.isEmpty(parent))
            nextItemsUrl = nextItemsUrl + "&parent=" + parent;

        GetRequest request = getKatoRequest(Constants.TRU_URL + nextItemsUrl);
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getKatoRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (listLoading) {
                            listLoading = false;
                            lvList.removeFooterView(footerView);
                        } else
                            hideProgress();
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONObject obj = new JSONObject(res.getString("object"));
                            JSONArray data = new JSONArray(obj.getString("data"));

                            int total = obj.getInt("total");
                            int count = obj.getInt("count");
                            int tempStart = obj.getInt("start");

                            if (count + tempStart < total) {
                                start = count + tempStart;
                            } else {
                                isEnd = true;
                            }

                            int index = truList.size();
                            for (int i=0;i<data.length();i++) {
                                JSONObject object = data.getJSONObject(i);
                                Tru tru = new Tru();
                                tru.setId(String.valueOf(index + i));
                                tru.setName(object.getString("name"));
                                tru.setLabel(object.getString("label"));
                                tru.setDescription(object.getString("descr"));
                                truList.add(tru);
                            }

                            if (truList.size() == 0) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("name", getIntent().getStringExtra("name"));
                                returnIntent.putExtra("descr", getIntent().getStringExtra("descr"));
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }
}
