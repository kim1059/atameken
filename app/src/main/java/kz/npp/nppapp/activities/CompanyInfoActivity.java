package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.CompanyBusiness;

public class CompanyInfoActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.llt_general_info)
    LinearLayout lltGeneralInfo;
    @BindView(R.id.llt_addresses)
    LinearLayout lltAddresses;
    @BindView(R.id.llt_telephones)
    LinearLayout lltTelephones;
    @BindView(R.id.llt_certificates)
    LinearLayout lltCertificates;
    @BindView(R.id.llt_show_map)
    LinearLayout lltShowMap;

    private SharedPreferences shared;
    private CompanyBusiness companyBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_info);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Компания");

        companyBusiness = (CompanyBusiness) getIntent().getSerializableExtra("company");
        tvName.setText(companyBusiness.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.llt_general_info)
    public void openGeneralInfo() {
        Intent intent = new Intent(CompanyInfoActivity.this, CompanySectionInfoActivity.class);
        intent.putExtra("id", "general_info");
        intent.putExtra("company", companyBusiness);
        startActivity(intent);
    }

    @OnClick(R.id.llt_addresses)
    public void openAddresses() {
        Intent intent = new Intent(CompanyInfoActivity.this, CompanySectionInfoActivity.class);
        intent.putExtra("id", "address");
        intent.putExtra("company", companyBusiness);
        startActivity(intent);
    }

    @OnClick(R.id.llt_certificates)
    public void openCertificates() {
        Intent intent = new Intent(CompanyInfoActivity.this, CertificatesActivity.class);
        intent.putExtra("text", "&producerBIN=" +companyBusiness.getBin());
        startActivity(intent);
    }

    @OnClick(R.id.llt_telephones)
    public void openTelephones() {
        Intent intent = new Intent(CompanyInfoActivity.this, CompanySectionInfoActivity.class);
        intent.putExtra("id", "telephones");
        intent.putExtra("company", companyBusiness);
        startActivity(intent);
    }

    @OnClick(R.id.llt_show_map)
    public void openMap() {
        Intent intent = new Intent(CompanyInfoActivity.this, MapActivity.class);
        intent.putExtra("lattitude", companyBusiness.getLattitude());
        intent.putExtra("longitude", companyBusiness.getLongitude());
        intent.putExtra("name", companyBusiness.getName());
        intent.putExtra("address", companyBusiness.getFactAddress());
        startActivity(intent);
    }
}
