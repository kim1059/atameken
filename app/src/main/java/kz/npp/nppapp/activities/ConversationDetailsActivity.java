package kz.npp.nppapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Conversation;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ConversationDetailsActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.llt_convo)
    LinearLayout lltConvo;
    @BindView(R.id.edt_text)
    EditText edtText;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private Conversation conversation;
    private SharedPreferences shared;
    private String textToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_details);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Беседа");

        conversation = (Conversation) getIntent().getSerializableExtra("conversation");
        setData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void setData() {
        tvName.setText(conversation.getServiceName());
        TreeMap<String, String> questions = conversation.getQuestions();
        TreeMap<String, String> questionDates = conversation.getQuestionsDate();
        TreeMap<String, String> answers = conversation.getAnswers();
        TreeMap<String, String> answerDates = conversation.getAnswersDate();

        Iterator it = questions.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();

            final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_question, lltConvo, false);
            TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_question);
            TextView tvDate = (TextView) inflatedLayout.findViewById(R.id.tv_date);
            tvText.setText(pair.getValue().toString());
            if (questionDates.get(pair.getKey().toString()) != null) {
                tvDate.setText(questionDates.get(pair.getKey().toString()));
            } else {
                tvDate.setText("Не указана");
            }

            lltConvo.addView(inflatedLayout);

            final View inflatedLayout2= getLayoutInflater().inflate(R.layout.row_answer, lltConvo, false);
            TextView tvText2 = (TextView) inflatedLayout2.findViewById(R.id.tv_answer);
            TextView tvDate2 = (TextView) inflatedLayout2.findViewById(R.id.tv_date);
            if (answerDates.get(pair.getKey().toString()) != null) {
                tvDate2.setText(answerDates.get(pair.getKey().toString()));
            } else {
                tvDate2.setText("");
            }
            if (answers.get(pair.getKey().toString()) != null) {
                tvText2.setText(answers.get(pair.getKey().toString()));
            } else {
                tvText2.setText("Пока не ответили");
            }
            lltConvo.addView(inflatedLayout2);
        }

    }

    @OnClick(R.id.iv_send)
    public void send() {
        if (edtText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Введите текст для отправки", Toast.LENGTH_LONG).show();
            return;
        }
        textToSend = edtText.getText().toString();
        edtText.setText("");
        Utils.hideSoftKeyboard(ConversationDetailsActivity.this);

        sendText();
    }

    private void sendText() {
        showProgress();
        getData(conversation.getUuid());
    }

    private void addNewQuestionToLayout(String date, String question) {
        final View inflatedLayout= getLayoutInflater().inflate(R.layout.row_question, lltConvo, false);
        TextView tvText = (TextView) inflatedLayout.findViewById(R.id.tv_question);
        TextView tvDate = (TextView) inflatedLayout.findViewById(R.id.tv_date);
        tvText.setText(question);
        tvDate.setText(date);

        lltConvo.addView(inflatedLayout);

        final View inflatedLayout2= getLayoutInflater().inflate(R.layout.row_answer, lltConvo, false);
        TextView tvText2 = (TextView) inflatedLayout2.findViewById(R.id.tv_answer);
        TextView tvDate2 = (TextView) inflatedLayout2.findViewById(R.id.tv_date);
        tvDate2.setText("");
        tvText2.setText("Пока не ответили");

        lltConvo.addView(inflatedLayout2);
    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            String justDate = Utils.getJustDate();
                            String dateAndTime = Utils.getDateAndTime();

                            JSONObject questionObject = new JSONObject();
                            JSONObject questionDateObject = new JSONObject();
                            JSONObject clientObject = new JSONObject();
                            JSONObject serviceObject = new JSONObject();
                            JSONObject statusObject = new JSONObject();
                            JSONObject convoObject = new JSONObject();
                            JSONObject question_problem = new JSONObject();
                            JSONArray dataArray = new JSONArray();

                            for (int i=0;i<data.length();i++) {
                                JSONObject conversationObject = data.getJSONObject(i);
                                if (conversationObject.getString("id").equalsIgnoreCase("client")) {
                                    clientObject = conversationObject;
                                } else if (conversationObject.getString("id").equalsIgnoreCase("name_service")) {
                                    serviceObject = conversationObject;
                                } else if (conversationObject.getString("id").equalsIgnoreCase("status_conversation")) {
                                    statusObject = conversationObject;
                                } else if (conversationObject.getString("id").equalsIgnoreCase("question_problem")) {
                                    question_problem = conversationObject;
                                } else if (conversationObject.getString("id").equalsIgnoreCase("table_conversation")) {
                                    String key = conversationObject.getString("key");
                                    key += "<div class='asf-Inline' style='font-family: Arial, sans-serif; font-size: 12px;'>"+dateAndTime+"</div>";
                                    key += "<div class='asf-Inline' style='font-family: Arial, sans-serif; font-size: 12px;'>"+textToSend+"</div>";

                                    questionDateObject.put("id", "question_date-b"+(conversation.getQuestions().size()+1));
                                    questionDateObject.put("type", "date");
                                    questionDateObject.put("value", justDate);
                                    questionDateObject.put("key", dateAndTime);

                                    questionObject.put("id", "question-b"+(conversation.getQuestions().size()+1));
                                    questionObject.put("type", "textbox");
                                    questionObject.put("value", textToSend);

                                    JSONArray jsonArray = new JSONArray(conversationObject.getString("data"));
                                    jsonArray.put(questionDateObject);
                                    jsonArray.put(questionObject);

                                    convoObject.put("id", "table_conversation");
                                    convoObject.put("type", "appendable_table");
                                    convoObject.put("key", key);
                                    convoObject.put("data", jsonArray);
                                }
                            }

                            dataArray.put(clientObject);
                            dataArray.put(serviceObject);
                            dataArray.put(statusObject);
                            dataArray.put(question_problem);
                            dataArray.put(convoObject);

                            addNewQuestionToLayout(justDate, textToSend);

                            String dataToSend = "\"data\":" + dataArray.toString();
                            Log.e("data to send ", dataToSend);

                            PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
                            request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
                            request.setData("77d37e6e-299f-4e1e-a6fd-613501b7a1c5", conversation.getUuid(), dataToSend);
                            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        lltConvo.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        lltConvo.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }
}
