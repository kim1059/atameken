package kz.npp.nppapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import kz.npp.nppapp.R;
import kz.npp.nppapp.fragments.BesedyFragment;
import kz.npp.nppapp.fragments.ContactsFragment;
import kz.npp.nppapp.fragments.MyCompaniesFragment;
import kz.npp.nppapp.fragments.MyFilesFragment;
import kz.npp.nppapp.fragments.MyOrdersFragment;
import kz.npp.nppapp.fragments.ServicesFragment;
import kz.npp.nppapp.fragments.SettingsFragment;
import kz.npp.nppapp.fragments.WantServiceFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean viewIsAtHome;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        if (getIntent().getBooleanExtra("from_push", false)) {
            Log.e("aa", "bb");
            if (getIntent().getStringExtra("type").equalsIgnoreCase("order_status")) {
                Log.e("aa", "cc");
                Intent intent = new Intent(MainActivity.this, MyOrderDetailsActivity.class);
                intent.putExtra("order_id", getIntent().getStringExtra("order_id"));
                intent.putExtra("from_push", true);
                startActivity(intent);
            } else {
                Log.e("aa", "dd");
                navigationView.setCheckedItem(R.id.nav_services);
                displayView(R.id.nav_services);
            }
        } else {
            Log.e("aa", "ee");
            navigationView.setCheckedItem(R.id.nav_services);
            displayView(R.id.nav_services);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (!viewIsAtHome) {
            navigationView.setCheckedItem(R.id.nav_services);
            displayView(R.id.nav_services);
        } else {
            Intent intent = new Intent(MainActivity.this, SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
        }
    }

    public void displayView(int viewId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String title = "";

        switch (viewId) {
            case R.id.nav_services:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, ServicesFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Услуги";
                viewIsAtHome = true;
                break;
            case R.id.nav_orders:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, MyOrdersFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Мои заявки";
                viewIsAtHome = false;
                break;
            case R.id.nav_besedy:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, BesedyFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Беседы";
                viewIsAtHome = false;
                break;
            case R.id.nav_my_companies:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, MyCompaniesFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Мои компании";
                viewIsAtHome = false;
                break;
            case R.id.nav_my_files:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, MyFilesFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Хранилище";
                viewIsAtHome = false;
                break;
            case R.id.nav_want_service:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, WantServiceFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Хочу услугу";
                viewIsAtHome = false;
                break;
            case R.id.nav_contacts:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, ContactsFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Контакты";
                viewIsAtHome = false;
                break;
            case R.id.nav_exit:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, SettingsFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = "Настройки";
                viewIsAtHome = false;
                break;
        }

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayView(item.getItemId());
        return true;
    }
}
