package kz.npp.nppapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class AskQuestion extends AppCompatActivity {

    @BindView(R.id.edt_question)
    EditText edtQuestion;
    @BindView(R.id.btn_ask)
    Button btnAsk;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private SharedPreferences shared;

    private String serviceName;
    private String serviceDocId;
    private String dataToSend;

    private boolean isProblem;
    private boolean isOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
//zapolnote anketu
        serviceName = getIntent().getStringExtra("service_name");
       // serviceDocId = getIntent().getStringExtra("service_doc_id");
        isProblem = getIntent().getBooleanExtra("is_problem", false);
        isOrder = getIntent().getBooleanExtra("is_order", false);

        if (isProblem) {
            tvTitle.setText("Проблема");
            edtQuestion.setHint("Сообщить о проблеме");
        } else {
            tvTitle.setText("Вопрос");
            edtQuestion.setHint("Задать вопрос");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_ask)
    public void ask() {
        showProgress();
        getServices();
    }

    private void doAsk() {
        if (edtQuestion.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Введите текст для отправки", Toast.LENGTH_LONG).show();
            hideProgress();
            return;
        }
        String questionText = edtQuestion.getText().toString();
        JSONObject clientObject = new JSONObject();
        JSONObject serviceObject = new JSONObject();
        JSONObject statusObject = new JSONObject();
        JSONObject convoObject = new JSONObject();
        JSONObject questionObject = new JSONObject();
        JSONObject questionDateObject = new JSONObject();
        JSONObject question_problem = new JSONObject();
        JSONArray dataArray = new JSONArray();

        try {
            clientObject.put("id", "client");
            clientObject.put("type", "reglink");
            clientObject.put("value", shared.getString(Constants.USER_ID, ""));
            clientObject.put("key", shared.getString(Constants.USER_DOC_ID, ""));
            clientObject.put("valueID", shared.getString(Constants.USER_DOC_ID, ""));
            clientObject.put("username", "Admin Admin Admin");
            clientObject.put("userID", "1");

            serviceObject.put("id", "name_service");
            serviceObject.put("type", "reglink");
            serviceObject.put("value", serviceName);
            serviceObject.put("key", serviceDocId);
            serviceObject.put("valueID", serviceDocId);
            serviceObject.put("username", "Admin Admin Admin");
            serviceObject.put("userID", "1");

            statusObject.put("id", "status_conversation");
            statusObject.put("type", "listbox");
            statusObject.put("value", "Новая");
            statusObject.put("key", "1");

            question_problem.put("id", "question_problem");
            question_problem.put("type", "listbox");
            if(isProblem) {
                question_problem.put("value", "Проблема");
                question_problem.put("key", "2");
            } else if(isOrder){
                question_problem.put("value", "Запрос по заявке");
                question_problem.put("key", "3");
            } else {
                question_problem.put("value", "Вопрос");
                question_problem.put("key", "1");
            }


            questionObject.put("id", "question-b1");
            questionObject.put("type", "textbox");
            questionObject.put("value", questionText);

            questionDateObject.put("id", "question_date-b1");
            questionDateObject.put("type", "date");
            questionDateObject.put("value", Utils.getJustDate());
            questionDateObject.put("key", Utils.getDateAndTime());

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(questionDateObject);
            jsonArray.put(questionObject);

            convoObject.put("id", "table_conversation");
            convoObject.put("type", "appendable_table");
            convoObject.put("key", ("<div class='asf-Inline' style='font-family: Arial, sans-serif; font-size: " +
                    "12px;'>")+Utils.getDateAndTime()+"</div>"+("<div class='asf-Inline' style='font-family: Arial, sans-serif;" +
                    " font-size: 12px;'>")+questionText+"</div>");
            convoObject.put("data", jsonArray);

            dataArray.put(clientObject);
            dataArray.put(serviceObject);
            dataArray.put(statusObject);
            dataArray.put(convoObject);
            dataArray.put(question_problem);

            dataToSend = "\"data\":" + dataArray.toString();

            createDoc("26940a10-bfdf-423b-b772-9a01647f6d09");
        } catch (JSONException e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    private void getServices() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("name_service");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='e6fc184a-8258-4be1-bfbd-ed42b132ef77' AND name_service LIKE '"+serviceName+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }



    private void createDoc(String uuid) {
        GetRequest request = createDocRequest(Constants.CREATE_DOC_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void saveData(String uuid) {
        PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData("77d37e6e-299f-4e1e-a6fd-613501b7a1c5", uuid, dataToSend);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                serviceDocId = jsonObject.getString("documentID");
                                doAsk();
                            }
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest createDocRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String error = res.getString("errorCode");
                            if (!error.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Не удалось создать пустую запись", Toast.LENGTH_LONG).show();
                                return;
                            }

                            saveData(res.getString("dataUUID"));
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Вопрос отправлен", Toast.LENGTH_LONG).show();
                        finish();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
        btnAsk.setVisibility(View.GONE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        btnAsk.setVisibility(View.VISIBLE);
    }
}
