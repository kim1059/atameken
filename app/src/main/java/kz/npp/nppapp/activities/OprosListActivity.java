package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.OprosAdapter;
import kz.npp.nppapp.models.Opros;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class OprosListActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Opros> adapter;
    private List<Opros> oprosList = new ArrayList<>();
    private SharedPreferences shared;

    private String orderUuid;
    //private String serviceNum;
    private String serviceName;
    private String serviceDocId;
    private String selectedOprosValue;
    private String registryUUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opros_list);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Опросы");

        orderUuid = getIntent().getStringExtra("order_uuid");
        //serviceNum = getIntent().getStringExtra("service_num");
        serviceName = getIntent().getStringExtra("service_name");

        showProgress();
        getOpros();

        adapter = new OprosAdapter(getApplicationContext(),
                R.layout.activity_companies_result_list,oprosList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedOprosValue = oprosList.get(position).getValue();
                showProgress();
                getServices();
                /*Intent intent = new Intent(OprosListActivity.this, CompanyInfoActivity.class);
                intent.putExtra("company", companyList.get(position));
                startActivity(intent);*/
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getOpros() {
        String query;
        if (Constants.isNpp) { // if test 1
            query = "where uuid='e680e9e8-1ed9-43d3-9ad2-2179ec2e4bf0' AND service_name like'"+serviceName+"'";
        } else {
            query = "where uuid='e8e0bb6b-f4cb-4d27-90b0-288bfe3d2e6b' AND service_name like'"+serviceName+"'";
        }
        ArrayList<String> list = new ArrayList<String>();
        list.add("service_name");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", query);
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Log.e("getopros", allInfo.toString());
        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getRegistryId() {
        Log.e("registry11",Constants.SIMPLE_SEARCH_URL + "?formUUID="+selectedOprosValue+"&recordsCount=1");
        GetRequest request = getRegistryRequest(Constants.SIMPLE_SEARCH_URL + "?formUUID="+selectedOprosValue+"&recordsCount=1");
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getOprosData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getRegistryId2(String uuid) {
        GetRequest request = getRegistry2Request(Constants.GET_DATA_URL + "document?dataUUID="+uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }


    private void getServices() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("name_service");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='e6fc184a-8258-4be1-bfbd-ed42b132ef77' AND name_service LIKE '"+serviceName+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchServicesRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchServicesRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                serviceDocId = jsonObject.getString("documentID");
                                getRegistryId();
                            }
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getRegistryRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response registry1",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                registryUUID= jsonArray.getString(i);
                                getRegistryId2(registryUUID);
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Не удалось получить id реестра", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("searchrequest",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getOprosData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Опросов нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("anketa")) {
                                    Opros opros = new Opros();
                                    opros.setName("Опрос " + (oprosList.size()+1));
                                    opros.setValue(object.getString("value"));
                                    oprosList.add(opros);
                                }

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getRegistry2Request(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject data = new JSONObject(response);
                            JSONObject jsonObject = new JSONObject(data.getString(registryUUID));
                            String servUrl;
                            if (Constants.isNpp)
                                servUrl = "http://services.palata.kz/services/cabinet/anketa2.php?";
                            else
                                servUrl = "http://test3.arta.pro/services/cabinet/anketa2.php?";
                            String url = servUrl +
                                    "id="+orderUuid+"&" +
                                    "form_id="+selectedOprosValue+"&" +
                                    "r_id="+jsonObject.getString("registryID")+"&" +
                                    "user="+shared.getString(Constants.LOGIN, "")+"&pass="+shared.getString(Constants.PASS, "")+"";
                            Intent intent = new Intent(OprosListActivity.this, OprosWebViewActivity.class);
                            intent.putExtra("url", url);
                            startActivity(intent);

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        lvList.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        lvList.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }
}
