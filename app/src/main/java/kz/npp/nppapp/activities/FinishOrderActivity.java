package kz.npp.nppapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class FinishOrderActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_service_name)
    TextView tvServiceName;
    @BindView(R.id.tv_company_name)
    TextView tvCompanyName;
    @BindView(R.id.tv_detalization)
    TextView tvDetalization;
    @BindView(R.id.llt_finish_order)
    LinearLayout lltFinishOrder;
    @BindView(R.id.view_bottom)
    View vBottom;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private SharedPreferences shared;
    private final int COMPANY_CHOOSE_CODE = 100;
    private final int DETALIZATION_CHOOSE_CODE = 200;

    private String companyValue, companyValueId, companyKey;
    private String newDocUUID;
    private String clientDocumentId;
    private String serviceDocumentId;
    private String detalizationKey;
    private String serviceName;
    private String serviceNum;
    private String companyBin;

    private boolean isCompanySelected;
    private boolean isDetalizationSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_order);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Проверка заполнения");

        String name = shared.getString(Constants.FIRST_NAME, "")+" "+ shared.getString(Constants.LAST_NAME, "");
        String email = shared.getString(Constants.MAIL, "");
        if (TextUtils.isEmpty(email))
            email = "email не указан";

        serviceName = getIntent().getStringExtra("service");
        serviceNum = getIntent().getStringExtra("service_num");
        serviceDocumentId = getIntent().getStringExtra("document_id");

        tvName.setText(name);
        tvEmail.setText(email);
        tvServiceName.setText(serviceName);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COMPANY_CHOOSE_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                isCompanySelected= true;
                companyValue = data.getStringExtra("value");
                companyValueId = data.getStringExtra("value_id");
                companyKey = data.getStringExtra("key");
                clientDocumentId = data.getStringExtra("document_id");
                tvCompanyName.setText(companyValue);
            }
        } else if (requestCode == DETALIZATION_CHOOSE_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                isDetalizationSelected= true;
                detalizationKey = data.getStringExtra("key");
                tvDetalization.setText(data.getStringExtra("name"));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.llt_finish_order)
    public void finishOrder() {
        getCompanyBin();
        //createDoc("27224991-2218-44b5-aa90-ead55f51affa");
    }

    @OnClick(R.id.iv_choose_service)
    public void chooseService() {
        Intent intent = new Intent(FinishOrderActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llt_choose_company)
    public void chooseCompany() {
        Intent intent = new Intent(FinishOrderActivity.this, ChooseCompanyActivity.class);
        startActivityForResult(intent, COMPANY_CHOOSE_CODE);
    }

    @OnClick(R.id.llt_choose_detalization)
    public void chooseDetalization() {
        Intent intent = new Intent(FinishOrderActivity.this, ChooseDetalizationActivity.class);
        intent.putExtra("service_name", serviceName);
        startActivityForResult(intent, DETALIZATION_CHOOSE_CODE);
    }

    private void createBussinessOrder() {
        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("client_id", shared.getString(Constants.USER_ID, ""));
            allInfo.put("client_company_bin", companyBin);
            allInfo.put("service_number", serviceNum);
            allInfo.put("detalization", detalizationKey);
            allInfo.put("determine_executor", 1);
            allInfo.put("source", "2");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = createBussinessRequest(Constants.NEW_ORDER_URL);
        request.setXauthKey(Constants.XAUTHKEY);
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getCompanyBin() {
        if (!isCompanySelected) {
            Toast.makeText(getApplicationContext(), "Выберите компанию", Toast.LENGTH_LONG).show();
            return;
        }

        if (!isDetalizationSelected) {
            Toast.makeText(getApplicationContext(), "Выберите детализацию", Toast.LENGTH_LONG).show();
            return;
        }

        showProgress();

        ArrayList<String> list = new ArrayList<String>();

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='297fcb61-3bc1-4b80-965f-b8ddfccf1270' AND documentID LIKE '"+companyKey+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Log.e("bin",allInfo.toString());
        PostRequest request = companyBinRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getCompanyBinData(String uuid) {
        GetRequest request = getCompanyBinRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void assignExecutor(String id) {
        PostRequest request = assignExecutorRequest(Constants.MY_APPLICATIONS_URL + id + "/executor/assign_from_location");
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void createDoc(String uuid) {
        if (!isCompanySelected) {
            Toast.makeText(getApplicationContext(), "Выберите компанию", Toast.LENGTH_LONG).show();
            return;
        }

        showProgress();
        GetRequest request = createDocRequest(Constants.CREATE_DOC_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void saveData() {
        Log.e("docid", newDocUUID);

        String companyEscape = companyValue.replace("\"", "\\\"");
        String data;
        data = "\"data\":[{\"id\":\"date\", \"value\": \""+getJustDate()+"\", \"key\" : \""+getDateAndTime()+"\", \"type\":\"date\" }," +
                "{\"id\":\"client\", \"username\":\"Admin Admin Admin\", \"userID\":\""+Constants.USER_ID+"\", \"value\": \"Документ\", \"type\": \"reglink\", \"key\": \""+clientDocumentId+"\", \"valueID\":\""+clientDocumentId+"\"}," +
                "{\"id\":\"position\", \"value\":\"Заместитель руководителя депаратамента\", \"type\":\"textbox\"}, " +
                "{\"id\":\"name_service\", \"username\":\"Admin Admin Admin\", \"userID\":\""+Constants.USER_ID+"\", \"value\": \""+serviceName+"\", \"type\": \"reglink\", \"key\": \""+serviceDocumentId+"\", \"valueID\":\""+serviceDocumentId+"\"}," +
                "{\"id\":\"company\", \"username\":\"Admin Admin Admin\", \"userID\":\""+Constants.USER_ID+"\", \"value\": \""+companyEscape+"\", \"type\": \"reglink\", \"key\": \""+companyKey+"\", \"valueID\":\""+companyValueId+"\"}]";

        Log.e("data", data);
        PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData("582ce05d-5b10-4639-bd0b-865993061b86", newDocUUID, data);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private String getJustDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    private String getDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }

    private GetRequest createDocRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String error = res.getString("errorCode");
                            if (!error.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Не удалось создать пустую запись", Toast.LENGTH_LONG).show();
                                return;
                            }

                            newDocUUID = res.getString("dataUUID");
                            saveData();
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response",response);
                        hideProgress();
                        Intent intent = new Intent(FinishOrderActivity.this, RateActivity.class);
                        startActivity(intent);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest createBussinessRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                           // hideProgress();
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String code = res.getString("code");
                            if (!code.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();
                                return;
                            }

                            /*get the datauuid of order*/
                            JSONObject dataObject = new JSONObject(res.getString("object"));

                            //createDoc("27224991-2218-44b5-aa90-ead55f51affa");

                            /*assign executor*/
                            assignExecutor(dataObject.getString("id"));

                            Intent intent = new Intent(FinishOrderActivity.this, RateActivity.class);
                            intent.putExtra("data_uuid", dataObject.getString("dataUUID"));
                            startActivity(intent);
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest companyBinRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getCompanyBinData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Не удалось найти БИН компании", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getCompanyBinRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("iin_bin")) {
                                    companyBin = object.getString("value");
                                }
                            }

                            createBussinessOrder();
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest assignExecutorRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("assign executor", response);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                    }
                });
    }

    private void hideProgress(){
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        lltFinishOrder.setVisibility(View.VISIBLE);
        vBottom.setVisibility(View.VISIBLE);
    }

    private void showProgress(){
        pbProgress.setVisibility(View.VISIBLE);
        lltFinishOrder.setVisibility(View.GONE);
        vBottom.setVisibility(View.GONE);
    }
}
