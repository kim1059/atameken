package kz.npp.nppapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;

public class CertificateSearch extends AppCompatActivity {

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_kpved)
    EditText edtKpved;
    @BindView(R.id.edt_product_name)
    EditText edtProductName;
    @BindView(R.id.edt_cert_num)
    EditText edtCertNum;
    @BindView(R.id.edt_blank_num)
    EditText edtBlankNum;
    @BindView(R.id.edt_bin)
    EditText edtBin;
    @BindView(R.id.edt_company_name)
    EditText edtCompanyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificate_search);
        ButterKnife.bind(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Поиск сертификатов");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_search)
    public void doSearch() {
        String text = "";
        if (!edtBin.getText().toString().isEmpty())
            text += "&producerBIN=" + edtBin.getText().toString();
        if (!edtBlankNum.getText().toString().isEmpty())
            text = edtBlankNum.getText().toString();
        if (!edtCertNum.getText().toString().isEmpty())
            text += "&certNumber=" +  edtCertNum.getText().toString();
        if (!edtCompanyName.getText().toString().isEmpty())
            text += "&producerName=" +  edtCompanyName.getText().toString();
        if (!edtKpved.getText().toString().isEmpty())
            text += "&kpved=" + edtKpved.getText().toString();
        if (!edtName.getText().toString().isEmpty())
            text = edtName.getText().toString();
        if (!edtProductName.getText().toString().isEmpty())
            text += "&searchTxt=" + edtProductName.getText().toString();

        Intent intent = new Intent(CertificateSearch.this, CertificatesActivity.class);
        intent.putExtra("text", text);
        startActivity(intent);
    }

}
