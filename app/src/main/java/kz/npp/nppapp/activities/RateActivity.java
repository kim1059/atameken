package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class RateActivity extends AppCompatActivity {

    @BindView(R.id.iv_rate_1)
    ImageView ivRate1;
    @BindView(R.id.iv_rate_2)
    ImageView ivRate2;
    @BindView(R.id.iv_rate_3)
    ImageView ivRate3;
    @BindView(R.id.iv_rate_4)
    ImageView ivRate4;
    @BindView(R.id.iv_rate_5)
    ImageView ivRate5;
    @BindView(R.id.btn_ok)
    Button btnOk;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private String rateCount = "4";
    private String dataUuid;

    private SharedPreferences shared;
    private String textToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        ButterKnife.bind(this);

        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Оцените нас");

        dataUuid = getIntent().getStringExtra("data_uuid");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_ok)
    public void goBack(){
        if (TextUtils.isEmpty(dataUuid)) {
            Intent intent = new Intent(RateActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            showProgress();
            getData(dataUuid);
        }


    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            String justDate = Utils.getJustDate();
                            String dateAndTime = Utils.getDateAndTime();

                            JSONObject dateObject = new JSONObject();
                            JSONObject clientObject = new JSONObject();
                            JSONObject positionObject = new JSONObject();
                            JSONObject serviceObject = new JSONObject();
                            JSONObject companyObject = new JSONObject();
                            JSONObject ratingObject = new JSONObject();
                            JSONArray dataArray = new JSONArray();

                            for (int i=0;i<data.length();i++) {
                                JSONObject object = data.getJSONObject(i);
                                dataArray.put(object);
                            }

                            ratingObject.put("id", "rating");
                            ratingObject.put("type", "textbox");
                            ratingObject.put("value", rateCount);

                           /* dataArray.put(clientObject);
                            dataArray.put(serviceObject);
                            dataArray.put(dateObject);
                            dataArray.put(positionObject);
                            dataArray.put(companyObject);*/
                            dataArray.put(ratingObject);

                            String dataToSend = "\"data\":" + dataArray.toString();
                            Log.e("data to send ", dataToSend);

                            PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
                            request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
                            request.setData("582ce05d-5b10-4639-bd0b-865993061b86", dataUuid, dataToSend);
                            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Intent intent = new Intent(RateActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        Intent intent = new Intent(RateActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Intent intent = new Intent(RateActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        btnOk.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        btnOk.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }

    @OnClick(R.id.iv_rate_1)
    public void rateOne() {
        rateCount = "1";
        ivRate1.setImageResource(R.drawable.rate_star_select_icon);
        ivRate2.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate3.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate4.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate5.setImageResource(R.drawable.rate_star_normal_icon);
    }

    @OnClick(R.id.iv_rate_2)
    public void rateTwo() {
        rateCount = "2";
        ivRate1.setImageResource(R.drawable.rate_star_select_icon);
        ivRate2.setImageResource(R.drawable.rate_star_select_icon);
        ivRate3.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate4.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate5.setImageResource(R.drawable.rate_star_normal_icon);
    }

    @OnClick(R.id.iv_rate_3)
    public void rateThree() {
        rateCount = "3";
        ivRate1.setImageResource(R.drawable.rate_star_select_icon);
        ivRate2.setImageResource(R.drawable.rate_star_select_icon);
        ivRate3.setImageResource(R.drawable.rate_star_select_icon);
        ivRate4.setImageResource(R.drawable.rate_star_normal_icon);
        ivRate5.setImageResource(R.drawable.rate_star_normal_icon);
    }

    @OnClick(R.id.iv_rate_4)
    public void rateFour() {
        rateCount = "4";
        ivRate1.setImageResource(R.drawable.rate_star_select_icon);
        ivRate2.setImageResource(R.drawable.rate_star_select_icon);
        ivRate3.setImageResource(R.drawable.rate_star_select_icon);
        ivRate4.setImageResource(R.drawable.rate_star_select_icon);
        ivRate5.setImageResource(R.drawable.rate_star_normal_icon);
    }

    @OnClick(R.id.iv_rate_5)
    public void rateFive() {
        rateCount = "5";
        ivRate1.setImageResource(R.drawable.rate_star_select_icon);
        ivRate2.setImageResource(R.drawable.rate_star_select_icon);
        ivRate3.setImageResource(R.drawable.rate_star_select_icon);
        ivRate4.setImageResource(R.drawable.rate_star_select_icon);
        ivRate5.setImageResource(R.drawable.rate_star_select_icon);
    }

}
