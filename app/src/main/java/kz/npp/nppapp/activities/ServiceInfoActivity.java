package kz.npp.nppapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Service;
import kz.npp.nppapp.utils.Constants;

public class ServiceInfoActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_short_desc)
    TextView tvShortDesc;
    @BindView(R.id.llt_question_result)
    LinearLayout lltQuestionResult;
    @BindView(R.id.llt_question_questions)
    LinearLayout lltQuestionQuestions;
    @BindView(R.id.llt_question_problems)
    LinearLayout lltQuestionProblems;
    @BindView(R.id.llt_question_laws)
    LinearLayout lltQuestionLaws;
    @BindView(R.id.llt_question_docs)
    LinearLayout lltQuestionDocs;
    @BindView(R.id.llt_question_internet)
    LinearLayout lltQuestionInternet;
    @BindView(R.id.llt_question_places)
    LinearLayout lltQuestionPlaces;
    @BindView(R.id.llt_question_order)
    LinearLayout lltQuestionOrder;

    private Service service;
    private String serviceNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_info);
        ButterKnife.bind(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Информация");

        Constants.FROM_INFO = true;
        service = (Service) getIntent().getSerializableExtra("service");
        serviceNum = getIntent().getStringExtra("service_num");

        if (service == null) {
            finish();
            return;
        }
        tvName.setText(service.getName());
        tvShortDesc.setText(service.getShortDesription());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
/*

    private void showImage() {
        String url = Constants.DOWNLOAD_IMAGE_URL + service.getImageUrl();
        Picasso.with(ServiceInfoActivity.this)
                .load(url)
                .error(R.drawable.squirrel)
                .into(ivAvatar);
    }
*/

    @OnClick(R.id.llt_question_result)
    public void openResult() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.RESULT_ID);
        intent.putExtra("service_name", service.getName());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_questions)
    public void openQuestions() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.QUESTIONS_ID);
        intent.putExtra("service_name", service.getName());
        intent.putExtra("service_doc_id", service.getDocumentId());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_problems)
    public void openProblems() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.PROBLEMS_ID);
        intent.putExtra("service_name", service.getName());
        intent.putExtra("service_doc_id", service.getDocumentId());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_laws)
    public void openLaws() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.LAWS_ID);
        intent.putExtra("service_name", service.getName());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_docs)
    public void openDocs() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.DOCS_ID);
        intent.putExtra("service_name", service.getName());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_internet)
    public void openInternet() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.INTERNET_ID);
        intent.putExtra("service_name", service.getName());
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_places)
    public void openPlaces() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceAddressesActivity.class);
        intent.putExtra("service_num", serviceNum);
        startActivity(intent);
    }

    @OnClick(R.id.llt_question_order)
    public void openOrder() {
        Intent intent = new Intent(ServiceInfoActivity.this, ServiceSectionInfo.class);
        intent.putExtra("id", Constants.ORDER_ID);
        intent.putExtra("service_name", service.getName());
        startActivity(intent);
    }

    @OnClick(R.id.llt_continue)
    public void doContinue() {
        Intent intent = new Intent(ServiceInfoActivity.this, FinishOrderActivity.class);
        intent.putExtra("service", service.getName());
        intent.putExtra("service_num", serviceNum);
        intent.putExtra("document_id", service.getDocumentId());
        startActivity(intent);
    }
}
