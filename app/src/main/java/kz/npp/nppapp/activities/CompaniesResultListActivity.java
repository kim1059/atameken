package kz.npp.nppapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.CompanyBusinessAdapter;
import kz.npp.nppapp.models.CompanyBusiness;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class CompaniesResultListActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<CompanyBusiness> adapter;
    private List<CompanyBusiness> companyList = new ArrayList<>();
    private SharedPreferences shared;

    private String name;
    private String kato;
    private String bin;
    private String tru;
    private boolean certificate;
    private View footerView;
    private boolean listLoading;
    private boolean hasNext = true;
    private int startRecord = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companies_result_list);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Компании");

        name = getIntent().getStringExtra("name");
        bin = getIntent().getStringExtra("bin");
        kato = getIntent().getStringExtra("kato");
        tru = getIntent().getStringExtra("tru");
        certificate = getIntent().getBooleanExtra("certificate", false);

        showProgress();
        getCompanies();

        adapter = new CompanyBusinessAdapter(getApplicationContext(),
                R.layout.activity_companies_result_list,companyList);
        footerView = ((LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_footer_progress, lvList, false);
        lvList.addFooterView(footerView);
        lvList.setAdapter(adapter);
        lvList.removeFooterView(footerView);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CompaniesResultListActivity.this, CompanyInfoActivity.class);
                intent.putExtra("company", companyList.get(position));
                startActivity(intent);
            }
        });

        lvList.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount > 19) {
                    if (!listLoading && hasNext) {
                        listLoading = true;
                        lvList.addFooterView(footerView);

                        getCompanies();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getCompanies() {
        String query = "where uuid='297fcb61-3bc1-4b80-965f-b8ddfccf1270' ";
        ArrayList<String> list = new ArrayList<String>();

        if (!TextUtils.isEmpty(name)) {
            query += "AND organization_name LIKE '%" + name +"%' ";
            list.add("organization_name");
        }

        if (!TextUtils.isEmpty(bin)) {
            query += "AND iin_bin LIKE '" + bin +"' ";
            list.add("iin_bin");
        }

        if (!TextUtils.isEmpty(kato)) {
            query += "AND kato LIKE '" + kato +"' ";
            list.add("kato");
        }

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", query);
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
            allInfo.put("recordsCount", 20);
            allInfo.put("startRecord", startRecord);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Log.e("getcompanies", allInfo.toString());
        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getCompanyData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("searchrequest",response);
                            if (listLoading) {
                                listLoading = false;
                                lvList.removeFooterView(footerView);
                            } else
                                hideProgress();
                            startRecord += 20;
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getCompanyData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Компаний нет", Toast.LENGTH_LONG).show();
                            }

                            if (jsonArray.length() < 20) {
                                hasNext = false;
                            }
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject date = null;
                            JSONObject bin = null;
                            JSONObject orgType = null;
                            JSONObject oked = null;
                            JSONObject extraOked = null;
                            JSONObject region = null;
                            JSONObject raion = null;
                            JSONObject kato = null;
                            JSONObject okpo = null;
                            JSONObject krp = null;
                            JSONObject name = null;
                            JSONObject urAddress = null;
                            JSONObject factAddress = null;
                            JSONObject lattitude = null;
                            JSONObject longitude = null;

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("organization_type")) {
                                    orgType = object;
                                } else if (object.getString("id").equals("organization_name")) {
                                    name = object;
                                } else if (object.getString("id").equals("iin_bin")) {
                                    bin = object;
                                } else if (object.getString("id").equals("kato")) {
                                    kato = object;
                                } else if (object.getString("id").equals("ur_adres")) {
                                    urAddress = object;
                                } else if (object.getString("id").equals("oked")) {
                                    oked = object;
                                } else if (object.getString("id").equals("vtor_oked")) {
                                    extraOked = object;
                                } else if (object.getString("id").equals("region")) {
                                    region = object;
                                } else if (object.getString("id").equals("rayon")) {
                                    raion = object;
                                } else if (object.getString("id").equals("krp_name")) {
                                    krp = object;
                                } else if (object.getString("id").equals("okpo")) {
                                    okpo = object;
                                } else if (object.getString("id").equals("registr_date")) {
                                    date = object;
                                } else if (object.getString("id").equals("shirota")) {
                                    lattitude = object;
                                } else if (object.getString("id").equals("dolgota")) {
                                    longitude = object;
                                } else if (object.getString("id").equals("t")) {
                                    JSONArray dataAddress = new JSONArray(object.getString("data"));
                                    for (int j=0; j<dataAddress.length(); j++) {
                                        JSONObject addressObject = dataAddress.getJSONObject(j);
                                        if (addressObject.getString("id").contains("fakt_adres")) {
                                            factAddress = addressObject;
                                        }
                                    }
                                }
                            }

                            CompanyBusiness company = new CompanyBusiness();
                            if (orgType != null && orgType.has("value")) {
                                company.setOrgType(name.getString("value"));
                            }
                            if (bin != null && bin.has("value")) {
                                company.setBin(bin.getString("value"));
                            }
                            if (name != null && name.has("value")) {
                                company.setName(name.getString("value"));
                            }
                            if (kato != null && kato.has("value")) {
                                company.setKato(kato.getString("value"));
                            }
                            if (urAddress != null && urAddress.has("value")) {
                                company.setUrAddress(urAddress.getString("value"));
                            }
                            if (oked != null && oked.has("value")) {
                                company.setOked(oked.getString("value"));
                            }
                            if (extraOked != null && extraOked.has("value")) {
                                company.setExtraOked(extraOked.getString("value"));
                            }
                            if (region != null && region.has("value")) {
                                company.setRegion(region.getString("value"));
                            }
                            if (raion != null && raion.has("value")) {
                                company.setRaion(raion.getString("value"));
                            }
                            if (krp != null && krp.has("key")) {
                                company.setKrp(krp.getString("key"));
                            }
                            if (okpo != null && okpo.has("key")) {
                                company.setOkpo(okpo.getString("key"));
                            }
                            if (date != null && date.has("key")) {
                                company.setDate(date.getString("key"));
                            }
                            if (factAddress != null && factAddress.has("value")) {
                                company.setFactAddress(factAddress.getString("value"));
                            }
                            if (lattitude != null && lattitude.has("value")) {
                                company.setLattitude(lattitude.getString("value"));
                            }
                            if (longitude != null && longitude.has("value")) {
                                company.setLongitude(longitude.getString("value"));
                            }

                            companyList.add(company);
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

}
