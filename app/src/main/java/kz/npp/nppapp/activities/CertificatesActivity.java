package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.CertificatesAdapter;
import kz.npp.nppapp.models.Certificate;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class CertificatesActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Certificate> adapter;
    private List<Certificate> certificateList = new ArrayList<>();
    private SharedPreferences shared;

    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificates);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Сертификаты");

        text = getIntent().getStringExtra("text");
        showProgress();
        /*get list of services*/
        getCertificates();

        adapter = new CertificatesAdapter(getApplicationContext(), R.layout.activity_certificates,certificateList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CertificatesActivity.this, CertificateInfoActivity.class);
                intent.putExtra("certificate", certificateList.get(position));
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getCertificates() {
        String searchText = TextUtils.isEmpty(text) ? "" : text;
        GetRequest request = getDataRequest(
                "http://82.200.165.100/api/cert/Get?lang=ru&size=20" + searchText);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }


    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("items"));

                            for (int i=0;i<data.length();i++) {
                                JSONObject certificateObject = data.getJSONObject(i);
                                Certificate  certificate = new Certificate();
                                certificate.setId(certificateObject.getString("id"));
                                //certificate.setDateExp(certificateObject.getString("dateExp"));
                                certificate.setDate(certificateObject.getString("date"));
                                certificate.setDescr(certificateObject.getString("descrRu"));
                                certificate.setMkei(certificateObject.getString("mkei"));
                                certificate.setTnved(certificateObject.getString("tnved"));
                                certificate.setKpved(certificateObject.getString("kpved"));
                                certificate.setNum(certificateObject.getString("num"));
                                certificate.setRegion(certificateObject.getString("regionRu"));
                                certificate.setCompany(certificateObject.getString("companyRu"));

                                certificateList.add(certificate);
                            }

                            if (certificateList.size() == 0)
                                Toast.makeText(getApplicationContext(), "Сертификатов нет", Toast.LENGTH_LONG).show();

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }
}
