package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.ServiceAddressAdapter;
import kz.npp.nppapp.models.MapAddress;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ServiceAddressesActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<MapAddress> adapter;
    private SharedPreferences shared;
    private String serviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_addresses);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Адреса");

        serviceId = getIntent().getStringExtra("service_num");
        Log.e("service", serviceId);

        showProgress();
        /*get list of services*/
        getAddresses();

        adapter = new ServiceAddressAdapter(getApplicationContext(), R.layout.activity_service_addresses, Constants.MAP_ADDRESS_LIST);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ServiceAddressesActivity.this, MapForServiceActivity.class);
                intent.putExtra("lattitude", Constants.MAP_ADDRESS_LIST.get(position).getLattitude());
                intent.putExtra("longitude", Constants.MAP_ADDRESS_LIST.get(position).getLongitude());
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getAddresses() {
        GetRequest request = getAddressesRequest(Constants.SERVICE_NPP_URL + serviceId + "/departments");
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private GetRequest getAddressesRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("res", response);
                            Constants.MAP_ADDRESS_LIST.clear();
                            hideProgress();
                            JSONObject obj = new JSONObject(response);
                            JSONArray objectArray = new JSONArray(obj.getString("object"));

                            for (int i=0; i<objectArray.length(); i++) {
                                JSONObject object = objectArray.getJSONObject(i);
                                JSONObject dataObject = new JSONObject(object.getString("data"));
                                JSONArray dataArray = new JSONArray(dataObject.getString("data"));

                                MapAddress mapAddress = new MapAddress();
                                for (int j=0; j<dataArray.length(); j++) {
                                    JSONObject object1 = dataArray.getJSONObject(j);
                                    if (object1.getString("id").equalsIgnoreCase("adres")) {
                                        mapAddress.setAddress(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("t")) {
                                        JSONArray telArray = new JSONArray(object1.getString("data"));
                                        String code = "";
                                        String cityCode = "";
                                        String phone = "";
                                        for (int k=0; k<telArray.length(); k++) {
                                            JSONObject object2 = telArray.getJSONObject(k);
                                            if (object2.getString("id").equalsIgnoreCase("city_code-")) {
                                                cityCode = object2.getString("value");
                                            } else if (object2.getString("id").equalsIgnoreCase("code-")) {
                                                code = object2.getString("value");
                                            } else if (object2.getString("id").equalsIgnoreCase("phone-")) {
                                                phone = object2.getString("value");
                                            }
                                        }
                                        mapAddress.setPhone(code+cityCode+phone);
                                    } else if (object1.getString("id").equalsIgnoreCase("email")) {
                                        mapAddress.setEmail(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("hours_1")) {
                                        mapAddress.setMondayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_1")) {
                                        mapAddress.setMondayFrom(mapAddress.getMondayFrom() + object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("hours_8")) {
                                        mapAddress.setMondayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_8")) {
                                        mapAddress.setMondayTo(mapAddress.getMondayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_2")) {
                                        mapAddress.setTuesdayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_2")) {
                                        mapAddress.setTuesdayFrom(mapAddress.getTuesdayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_9")) {
                                        mapAddress.setTuesdayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_9")) {
                                        mapAddress.setTuesdayTo(mapAddress.getTuesdayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_3")) {
                                        mapAddress.setWednesdayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_3")) {
                                        mapAddress.setWednesdayFrom(mapAddress.getWednesdayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_10")) {
                                        mapAddress.setWednesdayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_10")) {
                                        mapAddress.setWednesdayTo(mapAddress.getWednesdayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_4")) {
                                        mapAddress.setThursdayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_4")) {
                                        mapAddress.setThursdayFrom(mapAddress.getThursdayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_11")) {
                                        mapAddress.setThursdayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_11")) {
                                        mapAddress.setThursdayTo(mapAddress.getThursdayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_5")) {
                                        mapAddress.setFridayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_5")) {
                                        mapAddress.setFridayFrom(mapAddress.getFridayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_12")) {
                                        mapAddress.setFridayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_12")) {
                                        mapAddress.setFridayTo(mapAddress.getFridayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_6")) {
                                        mapAddress.setSaturdayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_6")) {
                                        mapAddress.setSaturdayFrom(mapAddress.getSaturdayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_13")) {
                                        mapAddress.setSaturdayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_13")) {
                                        mapAddress.setSaturdayTo(mapAddress.getSaturdayTo() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_7")) {
                                        mapAddress.setSundayFrom(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_7")) {
                                        mapAddress.setSundayFrom(mapAddress.getSundayFrom() + object1.getString("value"));
                                    }else if (object1.getString("id").equalsIgnoreCase("hours_14")) {
                                        mapAddress.setSundayTo(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("minutes_14")) {
                                        mapAddress.setSundayTo(mapAddress.getSundayTo() + object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("shirota")) {
                                        mapAddress.setLattitude(object1.getString("value"));
                                    } else if (object1.getString("id").equalsIgnoreCase("dolgota")) {
                                        mapAddress.setLongitude(object1.getString("value"));
                                    }
                                }

                                Constants.MAP_ADDRESS_LIST.add(mapAddress);
                            }
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }
}
