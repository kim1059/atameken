package kz.npp.nppapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Certificate;

public class CertificateInfoActivity extends AppCompatActivity {

    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_num)
    TextView tvNum;
    @BindView(R.id.tv_date_give)
    TextView tvDateGive;
    @BindView(R.id.tv_date_take)
    TextView tvDateTake;
    @BindView(R.id.tv_kpved)
    TextView tvKpved;
    @BindView(R.id.tv_tnved)
    TextView tvNved;
    @BindView(R.id.tv_mkei)
    TextView tvMkei;
    @BindView(R.id.tv_descr)
    TextView tvDescr;

    private SharedPreferences shared;
    private Certificate certificate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificate_info);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Сертификат");

        certificate = (Certificate) getIntent().getSerializableExtra("certificate");

        putInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void putInfo() {
        tvId.setText(certificate.getId());
        tvNum.setText(certificate.getNum());
        tvDateGive.setText(certificate.getDate());
       // tvDateTake.setText(certificate.getDateExp());
        tvKpved.setText(certificate.getKpved());
        tvNved.setText(certificate.getTnved());
        tvMkei.setText(certificate.getMkei());
        tvDescr.setText(certificate.getDescr());
    }
}
