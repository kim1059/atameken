package kz.npp.nppapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.npp.nppapp.R;
import kz.npp.nppapp.adapters.ServicesAdapter;
import kz.npp.nppapp.models.Service;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class ServicesListActivity extends AppCompatActivity {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Service> adapter;
    private List<Service> servicesList = new ArrayList<>();
    private SharedPreferences shared;
    private Service service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);
        ButterKnife.bind(this);
        shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Услуги");

        showProgress();
        /*get list of services*/
        getServices();

        service = (Service) getIntent().getSerializableExtra("service");

        adapter = new ServicesAdapter(getApplicationContext(), R.layout.activity_services_list,servicesList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (servicesList.get(position).getFormId() != null) {
                    startWebView(servicesList.get(position).getId());
                } else if (servicesList.get(position).getSystemReceiver().equalsIgnoreCase("1")) {
                    Intent intent = new Intent(getApplicationContext(), CompanySearchActivity.class);
                    intent.putExtra("service_num", servicesList.get(position).getServiceNum());
                    startActivity(intent);
                } else {
                    Constants.FROM_INFO = false;
                    Intent intent = new Intent(ServicesListActivity.this, ServicesListActivity.class);
                    intent.putExtra("service_num", servicesList.get(position).getServiceNum());
                    intent.putExtra("service", servicesList.get(position));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.FROM_INFO) {
            Constants.FROM_INFO = false;
            onBackPressed();
            Log.e("AAA", "On Resume .....");
        }
        Log.e("AAA", "On Resume .....");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getServices() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("num_parent_service");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='e6fc184a-8258-4be1-bfbd-ed42b132ef77' AND num_parent_service LIKE '"+getIntent().getStringExtra("service_num")+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void getServiceData(String uuid, String documentId) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid, documentId, uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getServiceData(jsonObject.getString("dataUUID"),
                                        jsonObject.getString("documentID"));
                            }
                            if (jsonArray.length() == 0) {
                                if (service.getSystemReceiver().equalsIgnoreCase("1")) {
                                    Intent intent = new Intent(getApplicationContext(), CompanySearchActivity.class);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(ServicesListActivity.this, ServiceInfoActivity.class);
                                    intent.putExtra("service", service);
                                    intent.putExtra("service_num",getIntent().getStringExtra("service_num"));
                                    startActivity(intent);
                                }
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url, final String documentId, final String uuid) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            JSONObject name = null;
                            JSONObject num = null;
                            JSONObject image = null;
                            JSONObject receiver = null;
                            JSONObject formId = null;

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("status_service")) {
                                    if (!object.getString("key").equalsIgnoreCase("1"))
                                        return;
                                } else if (object.getString("id").equals("name_service")) {
                                    name = object;
                                } else if (object.getString("id").equals("num_service")) {
                                    num = object;
                                } else if (object.getString("id").equals("image2")) {
                                    image = object;
                                } else if (object.getString("id").equals("system_receiver")) {
                                    receiver = object;
                                } else if (object.getString("id").equals("form_id")) {
                                    formId = object;
                                }
                            }

                            Service service = new Service();
                            service.setId(uuid);
                            if (name != null) {
                                service.setName(name.getString("value"));
                            }
                            if (num != null) {
                                service.setServiceNum(num.getString("value"));
                            }
                            if ((receiver != null) && receiver.has("key")) {
                                service.setSystemReceiver(receiver.getString("key"));
                            } else {
                                service.setSystemReceiver("100");
                            }
                            service.setDocumentId(documentId);
                            if (image != null && image.has("key"))
                                service.setImageUrl(image.getString("key"));
                            if (formId != null) {
                                if (formId.has("value") && !TextUtils.isEmpty(formId.getString("value")))
                                    service.setFormId(formId.getString("value"));
                            }
                            servicesList.add(service);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        /*if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 400) {
                                Toast.makeText(getApplicationContext(), "Не правильно введены данные", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Пользователь забанен или не активный", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        }*/
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void startWebView(String uuid) {
        String servUrl;
        if (Constants.isNpp)
            servUrl = "http://services.palata.kz/services/auto_service2.php?";
        else
            servUrl = "http://npp.arta.pro/services/auto_service2.php?";
        String url = servUrl +
                "uuid="+uuid+ "&" +
                "user="+shared.getString(Constants.LOGIN, "")+"&pass="+shared.getString(Constants.PASS, "")+"";
        Intent intent = new Intent(ServicesListActivity.this, OprosWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

}
