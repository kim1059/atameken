package kz.npp.nppapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.edt_first_name)
    EditText edtFirstName;
    @BindView(R.id.edt_last_name)
    EditText edtLastName;
    @BindView(R.id.edt_patronymic)
    EditText edtPatronymic;
    @BindView(R.id.edt_iin)
    EditText edtIin;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_pass)
    EditText edtPass;
    @BindView(R.id.edt_tel)
    EditText edtTel;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView tvTitle = (TextView) mToolbar.findViewById(R.id.tv_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvTitle.setText("Регистрация");

        edtTel.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - edtTel.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!editedFlag) {
                    if (phone.length() >= 6 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        edtTel.setText(ans);
                        edtTel.setSelection(edtTel.getText().length() - cursorComplement);
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        edtTel.setText(ans);
                        edtTel.setSelection(edtTel.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_register)
    public void doRegister() {
        String firstName = edtFirstName.getText().toString();
        String lastName = edtLastName.getText().toString();
        String patronymic = edtPatronymic.getText().toString();
        String email = edtEmail.getText().toString();
        String iin = edtIin.getText().toString();
        String pass = edtPass.getText().toString();
        String phone = edtTel.getText().toString().replaceAll("\\D", "");
        String url = Constants.REGISTRATION_CONFIRM_URL;

        Utils.hideSoftKeyboard(RegistrationActivity.this);

        // Reset errors.
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtPatronymic.setError(null);
        edtEmail.setError(null);
        edtIin.setError(null);
        edtPass.setError(null);
        edtTel.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(firstName)) {
            edtFirstName.setError(getString(R.string.can_not_be_null));
            focusView = edtFirstName;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            edtLastName.setError(getString(R.string.can_not_be_null));
            focusView = edtLastName;
            cancel = true;
        }

        if (TextUtils.isEmpty(phone) || !Utils.isPhoneValid(phone)) {
            edtTel.setError(getString(R.string.can_not_be_null));
            focusView = edtTel;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            edtEmail.setError(getString(R.string.can_not_be_null));
            focusView = edtEmail;
            cancel = true;
        }

        if (TextUtils.isEmpty(iin)) {
            edtIin.setError(getString(R.string.can_not_be_null));
            focusView = edtIin;
            cancel = true;
        }

        if (TextUtils.isEmpty(pass)) {
            edtPass.setError(getString(R.string.can_not_be_null));
            focusView = edtPass;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress();

            JSONObject allInfo = new JSONObject();
            try {
                allInfo.put("login", iin);
                allInfo.put("password", pass);
                allInfo.put("email", email);
                allInfo.put("firstname", firstName);
                allInfo.put("lastname", lastName);
                allInfo.put("patronymic", patronymic);
                allInfo.put("phone", phone);
                allInfo.put("source", "2");
               // allInfo.put("is_mobile", "true");
                allInfo.put("iin", iin);
                allInfo.put("url", url);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Неправильные json данные", Toast.LENGTH_LONG).show();
                e.printStackTrace();
                return;
            }

            PostRequest request = registerRequest(Constants.REGISTER_URL);
            request.setXauthKey(Constants.XAUTHKEY);
            request.setData(allInfo.toString());
            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
    }

    private PostRequest registerRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("reposne", response);
                        try {
                            JSONObject object = new JSONObject(response);
                             /*check for an error*/
                            String code = object.getString("code");
                            if (!code.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getApplicationContext(), "Произошла ошибка. Пользователь с этими данными уже существует", Toast.LENGTH_LONG).show();
                                return;
                            } else {
                                Toast.makeText(getApplicationContext(), "Регистрация прошла успешно. Проверьте email", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgress();
                        finish();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        btnRegister.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        btnRegister.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }

}
