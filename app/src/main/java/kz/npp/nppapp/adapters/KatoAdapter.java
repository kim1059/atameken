package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Kato;

/**
 * Created by kim1059 on 20.09.2016.
 */
public class KatoAdapter extends ArrayAdapter<Kato> {

    List<Kato> katoList;
    private Context context;

    public KatoAdapter(Context context, int resource, List<Kato> katoList) {
        super(context, resource, katoList);
        this.katoList = katoList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        Kato kato = katoList.get(position);
        tvName.setText(kato.getDisplayName());

        return convertView;
    }

}