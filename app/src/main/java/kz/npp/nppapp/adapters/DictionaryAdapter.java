package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Dictionary;

/**
 * Created by kim1059 on 28.09.2016.
 */
public class DictionaryAdapter extends ArrayAdapter<Dictionary> {

    List<Dictionary> dictionaryList;
    private Context context;

    public DictionaryAdapter(Context context, int resource, List<Dictionary> dictionaryList) {
        super(context, resource, dictionaryList);
        this.dictionaryList = dictionaryList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        Dictionary dictionary = dictionaryList.get(position);
        tvName.setText(dictionary.getName());

        return convertView;
    }

}