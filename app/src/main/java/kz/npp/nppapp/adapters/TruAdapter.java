package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Tru;

/**
 * Created by kim1059 on 29.09.2016.
 */
public class TruAdapter extends ArrayAdapter<Tru> {

    List<Tru> truList;
    private Context context;

    public TruAdapter(Context context, int resource, List<Tru> truList) {
        super(context, resource, truList);
        this.truList = truList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        Tru tru = truList.get(position);
        tvName.setText(tru.getName());

        return convertView;
    }

}