package kz.npp.nppapp.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Service;
import kz.npp.nppapp.utils.Constants;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class ServicesAdapter extends ArrayAdapter<Service> {

    List<Service> servicesList;
    private Context context;
    private SharedPreferences shared;
    private Picasso picasso;

    public ServicesAdapter(Context context, int resource, List<Service> servicesList) {
        super(context, resource, servicesList);
        this.servicesList = servicesList;
        this.context = context;
        shared = PreferenceManager.getDefaultSharedPreferences(context);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .authenticator(new Authenticator() {
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException
                    {
                        String credential = Credentials.basic(shared.getString(Constants.LOGIN, ""),
                                shared.getString(Constants.PASS, ""));
                        return response.request().newBuilder()
                                .header("Authorization", credential)
                                .build();
                    }
                })
                .build();

        picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .listener(new Picasso.Listener()
                {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
                    {
                        exception.printStackTrace();
                    }
                })
                .build();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_service, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        ImageView ivAvatar = (ImageView) convertView.findViewById(R.id.iv_avatar);

        Service service = servicesList.get(position);
        tvName.setText(service.getName());

        String url = Constants.DOWNLOAD_IMAGE_URL + service.getImageUrl();
        if (service.getImageUrl() != null) {
            showImage(url, ivAvatar);
        } else {
            ivAvatar.setImageResource(R.drawable.ic_no_photo);
        }

        return convertView;
    }

    private void showImage(String url, ImageView ivAvatar) {
        picasso
                .load(url)
                .error(R.drawable.ic_no_photo)
                .into(ivAvatar);
    }
}