package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Order;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class OrdersAdapter extends ArrayAdapter<Order> {

    List<Order> ordersList;
    private Context context;

    public OrdersAdapter(Context context, int resource, List<Order> ordersList) {
        super(context, resource, ordersList);
        this.ordersList = ordersList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_order, parent, false);
        TextView tvOrderName = (TextView) convertView.findViewById(R.id.tv_order_name);
        TextView tvServiceName = (TextView) convertView.findViewById(R.id.tv_service_name);
        TextView tvStatus = (TextView) convertView.findViewById(R.id.tv_status);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);

        Order order = ordersList.get(position);
        tvOrderName.setText(order.getOrderName());
        tvServiceName.setText(order.getServiceName());
        tvStatus.setText(order.getStatus());
        tvDate.setText(order.getDate());

        if (order.getStatus()!= null) {
            if (order.getStatus().equalsIgnoreCase("Новая"))
                convertView.setBackgroundResource(R.color.colorNew);
            else if (order.getStatus().equalsIgnoreCase("В работе"))
                convertView.setBackgroundResource(R.color.colorInProgress);
            else if (order.getStatus().equalsIgnoreCase("принята в работу"))
                convertView.setBackgroundResource(R.color.colorInProgress);
            else if (order.getStatus().equalsIgnoreCase("Исполнена"))
                convertView.setBackgroundResource(R.color.colorDone);
            else
                convertView.setBackgroundResource(R.color.colorWhite);
        }

        return convertView;
    }

}