package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.CompanyBusiness;

/**
 * Created by kim1059 on 19.09.2016.
 */
public class CompanyBusinessAdapter extends ArrayAdapter<CompanyBusiness> {

    List<CompanyBusiness> companiesList;
    private Context context;

    public CompanyBusinessAdapter(Context context, int resource, List<CompanyBusiness> companiesList) {
        super(context, resource, companiesList);
        this.companiesList = companiesList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        CompanyBusiness company = companiesList.get(position);
        tvName.setText(company.getName());

        return convertView;
    }

}