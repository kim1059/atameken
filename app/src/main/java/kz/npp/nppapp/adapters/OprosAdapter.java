package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Opros;

/**
 * Created by kim1059 on 14.10.2016.
 */
public class OprosAdapter extends ArrayAdapter<Opros> {

    List<Opros> oprosList;
    private Context context;

    public OprosAdapter(Context context, int resource, List<Opros> oprosList) {
        super(context, resource, oprosList);
        this.oprosList = oprosList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        Opros opros = oprosList.get(position);
        tvName.setText(opros.getName());

        return convertView;
    }

}