package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.File;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.DownloadTask;

/**
 * Created by kim1059 on 28.09.2016.
 */
public class FilesAdapter extends ArrayAdapter<File> {

    List<File> fileList;
    private Context context;

    public FilesAdapter(Context context, int resource, List<File> fileList) {
        super(context, resource, fileList);
        this.fileList = fileList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_file, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
        ImageView ivDownload = (ImageView) convertView.findViewById(R.id.iv_download);

        final File file = fileList.get(position);
        tvName.setText(file.getName());
        tvDate.setText(file.getDate());

        ivDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadTask downloadTask = new DownloadTask(context);
                downloadTask.execute(Constants.DOWNLOAD_IMAGE_URL + file.getUrl(), file.getFileName());
            }
        });

        return convertView;
    }
}
