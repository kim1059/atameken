package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Certificate;

/**
 * Created by kim1059 on 30.09.2016.
 */
public class CertificatesAdapter extends ArrayAdapter<Certificate> {

    List<Certificate> certificateList;
    private Context context;

    public CertificatesAdapter(Context context, int resource, List<Certificate> certificateList) {
        super(context, resource, certificateList);
        this.certificateList = certificateList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        Certificate certificate = certificateList.get(position);
        tvName.setText(certificate.getCompany());

        return convertView;
    }

}