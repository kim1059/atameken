package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.MapAddress;

/**
 * Created by kim1059 on 04.10.2016.
 */
public class ServiceAddressAdapter extends ArrayAdapter<MapAddress> {

    List<MapAddress> mapAddressList;
    private Context context;

    public ServiceAddressAdapter(Context context, int resource, List<MapAddress> mapAddressList) {
        super(context, resource, mapAddressList);
        this.mapAddressList = mapAddressList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_company, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);

        MapAddress mapAddress = mapAddressList.get(position);
        tvName.setText(mapAddress.getAddress());

        return convertView;
    }

}