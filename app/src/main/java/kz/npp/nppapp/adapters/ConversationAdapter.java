package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Conversation;

/**
 * Created by kim1059 on 22.09.2016.
 */
public class ConversationAdapter extends ArrayAdapter<Conversation> {

    List<Conversation> conversationList;
    private Context context;

    public ConversationAdapter(Context context, int resource, List<Conversation> conversationList) {
        super(context, resource, conversationList);
        this.conversationList = conversationList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_conversation, parent, false);
        TextView tvServiceName = (TextView) convertView.findViewById(R.id.tv_service_name);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
        TextView tvQuestion = (TextView) convertView.findViewById(R.id.tv_question);

        Conversation conversation = conversationList.get(position);
        tvServiceName.setText(conversation.getServiceName());
        tvDate.setText(conversation.getQuestionsDate().entrySet().iterator().next().getValue());
        tvQuestion.setText(conversation.getQuestions().entrySet().iterator().next().getValue());

        return convertView;
    }
}

