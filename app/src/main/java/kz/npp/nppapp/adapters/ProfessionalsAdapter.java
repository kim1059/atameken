package kz.npp.nppapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kz.npp.nppapp.R;
import kz.npp.nppapp.models.Professional;

/**
 * Created by kim1059 on 25.08.2016.
 */
public class ProfessionalsAdapter extends ArrayAdapter<Professional> {

    List<Professional> professionalsList;
    private Context context;

    public ProfessionalsAdapter(Context context, int resource, List<Professional> professionalsList) {
        super(context, resource, professionalsList);
        this.professionalsList = professionalsList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.row_professional, parent, false);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        ImageView ivAvatar = (ImageView) convertView.findViewById(R.id.iv_avatar);

        Professional professional = professionalsList.get(position);
        tvName.setText(professional.getName());

        if (professional.getAvatar() != null) {
            Picasso.with(context)
                    .load(professional.getAvatar())
                    .into(ivAvatar);
        } else {
            ivAvatar.setImageResource(R.drawable.squirrel);
        }
        return convertView;
    }

}