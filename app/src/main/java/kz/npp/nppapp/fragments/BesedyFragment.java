package kz.npp.nppapp.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.ConversationDetailsActivity;
import kz.npp.nppapp.adapters.ConversationAdapter;
import kz.npp.nppapp.models.Conversation;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class BesedyFragment extends Fragment {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.tv_sort_name)
    TextView tvSortName;

    private ArrayAdapter<Conversation> adapter;
    private List<Conversation> conversationList = new ArrayList<>();
    private SharedPreferences shared;

    ImageView ivAll, ivQuestions, ivProblems, ivFromOrder;
    int statusNum = 0;

    public BesedyFragment() {
        // Required empty public constructor
    }

    public static BesedyFragment newInstance() {
        BesedyFragment fragment = new BesedyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_besedy, container, false);
        ButterKnife.bind(this, rootView);
        shared = PreferenceManager.getDefaultSharedPreferences(getActivity());

        tvSortName.setText("Все");
        adapter = new ConversationAdapter(getActivity(), R.layout.fragment_besedy,conversationList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ConversationDetailsActivity.class);
                intent.putExtra("conversation", conversationList.get(position));
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        showProgress();
        /*get list of services*/
        getConversations();
        super.onResume();
    }

    @OnClick(R.id.llt_sort)
    public void doSort() {
        // create a Dialog component
        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_sort_conversation);

        LinearLayout lltQuestions = (LinearLayout) dialog.findViewById(R.id.llt_questions);
        LinearLayout lltProblems = (LinearLayout) dialog.findViewById(R.id.llt_problems);
        LinearLayout lltFromOrder = (LinearLayout) dialog.findViewById(R.id.llt_from_order);
        LinearLayout lltAll = (LinearLayout) dialog.findViewById(R.id.llt_all);

        ivQuestions = (ImageView) dialog.findViewById(R.id.iv_questions);
        ivProblems = (ImageView) dialog.findViewById(R.id.iv_problems);
        ivFromOrder = (ImageView) dialog.findViewById(R.id.iv_from_order);
        ivAll = (ImageView) dialog.findViewById(R.id.iv_all);

                /*put click on right position*/
        selectCurrentFilterOrder();

        lltAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 0;
                selectCurrentFilterOrder();

                dialog.dismiss();
                conversationList.clear();
                adapter.notifyDataSetChanged();
                getConversations();
            }
        });

        lltQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 1;
                selectCurrentFilterOrder();

                dialog.dismiss();
                conversationList.clear();
                adapter.notifyDataSetChanged();
                getConversations();
            }
        });
        lltProblems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 2;
                selectCurrentFilterOrder();

                dialog.dismiss();
                conversationList.clear();
                adapter.notifyDataSetChanged();
                getConversations();
            }
        });
        lltFromOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 3;
                selectCurrentFilterOrder();

                dialog.dismiss();
                conversationList.clear();
                adapter.notifyDataSetChanged();
                getConversations();
            }
        });
        dialog.show();
    }

    private void hideAllOkInFilterDialog() {
        ivQuestions.setVisibility(View.GONE);
        ivProblems.setVisibility(View.GONE);
        ivFromOrder.setVisibility(View.GONE);
        ivAll.setVisibility(View.GONE);
    }

    private void selectCurrentFilterOrder() {
        hideAllOkInFilterDialog();
        if (statusNum == 0) {
            tvSortName.setText("Все");
            ivAll.setVisibility(View.VISIBLE);
        }else if (statusNum == 1) {
            tvSortName.setText("Вопросы");
            ivQuestions.setVisibility(View.VISIBLE);
        } else if (statusNum == 2) {
            tvSortName.setText("Проблемы");
            ivProblems.setVisibility(View.VISIBLE);
        } else if (statusNum == 3) {
            tvSortName.setText("Запросы по заявке");
            ivFromOrder.setVisibility(View.VISIBLE);
        }
    }


    private void getConversations() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("client");
        String status = "";
        if (statusNum != 0) {
            list.add("question_problem");
            if (statusNum == 1) {
                status = " AND question_problem like 'Вопрос'";
            } else if (statusNum == 2) {
                status = " AND question_problem like 'Проблема'";
            } else {
                status = " AND question_problem like 'Запрос по заявке'";
            }
        }

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='77d37e6e-299f-4e1e-a6fd-613501b7a1c5'  AND client LIKE '"+ shared.getString(Constants.USER_ID, "")+"'"+status+"");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid, uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            conversationList.clear();
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getActivity(), "Бесед пока нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url, final String uuid) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            Conversation conversation = new Conversation();
                            conversation.setUuid(uuid);

                            TreeMap<String, String> questions = new TreeMap<>();
                            TreeMap<String, String> questionDates = new TreeMap<>();
                            TreeMap<String, String> answers = new TreeMap<>();
                            TreeMap<String, String> answerDates = new TreeMap<>();

                            for (int i=0;i<data.length();i++) {
                                JSONObject conversationObject = data.getJSONObject(i);
                                if (conversationObject.getString("id").equalsIgnoreCase("name_service")) {
                                    conversation.setServiceName(conversationObject.getString("value"));
                                    conversation.setNameServiceInfo(conversationObject.toString());
                                }

                                else if (conversationObject.getString("id").equalsIgnoreCase("table_conversation")) {
                                    JSONArray jsonArray = new JSONArray(conversationObject.getString("data"));
                                    for (int j=0;j<jsonArray.length(); j++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(j);
                                        if (jsonObject.getString("id").contains("question_date-")) {
                                            questionDates.put(jsonObject.getString("id").substring(14), jsonObject.getString("value"));
                                        } else if (jsonObject.getString("id").contains("question-")) {
                                            questions.put(jsonObject.getString("id").substring(9), jsonObject.getString("value"));
                                        } else if (jsonObject.getString("id").contains("answer_date-")) {
                                            answerDates.put(jsonObject.getString("id").substring(12), jsonObject.getString("value"));
                                        } else if (jsonObject.getString("id").contains("answer-")) {
                                            answers.put(jsonObject.getString("id").substring(7), jsonObject.getString("value"));
                                        }

                                    }
                                    conversation.setQuestions(questions);
                                    conversation.setQuestionsDate(questionDates);
                                    conversation.setAnswers(answers);
                                    conversation.setAnswersDate(answerDates);
                                    conversationList.add(conversation);
                                }
                            }

                            /*if (conversationList.size() == 0)
                                Toast.makeText(getActivity(), "Бесед пока нет", Toast.LENGTH_LONG).show();
*/
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        lvList.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        lvList.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }


}
