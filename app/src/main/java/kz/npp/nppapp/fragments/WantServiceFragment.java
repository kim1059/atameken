package kz.npp.nppapp.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.MainActivity;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostUrlEncodedRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class WantServiceFragment extends Fragment {

    @BindView(R.id.edt_service)
    EditText edtService;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;


    private String dataToSend;
    private SharedPreferences shared;

    public WantServiceFragment() {
        // Required empty public constructor
    }

    public static WantServiceFragment newInstance() {
        WantServiceFragment fragment = new WantServiceFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_want_service, container, false);
        ButterKnife.bind(this, rootView);
        shared = PreferenceManager.getDefaultSharedPreferences(getActivity());

        return rootView;
    }

    @OnClick(R.id.btn_send)
    public void doSend() {
        if (edtService.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Введите текст для отправки", Toast.LENGTH_LONG).show();
            hideProgress();
            return;
        }

        showProgress();
        Utils.hideSoftKeyboard(getActivity());

        String serviceText = edtService.getText().toString();
        JSONObject whoAskObject = new JSONObject();
        JSONObject serviceObject = new JSONObject();
        JSONArray dataArray = new JSONArray();

        try {
            serviceObject.put("id", "service");
            serviceObject.put("type", "textbox");
            serviceObject.put("value", serviceText);

            whoAskObject.put("id", "who_ask");
            whoAskObject.put("type", "entity");
            whoAskObject.put("value", shared.getString(Constants.FIRST_NAME,"")
                    + " " + shared.getString(Constants.LAST_NAME, ""));
            whoAskObject.put("key", shared.getString(Constants.USER_ID, ""));

            dataArray.put(whoAskObject);
            dataArray.put(serviceObject);

            dataToSend = "\"data\":" + dataArray.toString();

            createDoc("6280eed5-5f59-4fe8-9cfa-eb931ba4ea71");
        } catch (JSONException e) {
            e.printStackTrace();
            hideProgress();
        }
    }


    private void createDoc(String uuid) {
        GetRequest request = createDocRequest(Constants.CREATE_DOC_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void saveData(String uuid) {
        PostUrlEncodedRequest request = saveDataRequest(Constants.SAVE_DATA_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData("5efe13cf-813d-4fbb-878e-a2a0c4022d3a", uuid, dataToSend);
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }


    private GetRequest createDocRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);

                            /*check for an error*/
                            String error = res.getString("errorCode");
                            if (!error.equalsIgnoreCase("0")){
                                hideProgress();
                                Toast.makeText(getActivity(), "Не удалось создать пустую запись", Toast.LENGTH_LONG).show();
                                return;
                            }

                            saveData(res.getString("dataUUID"));
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostUrlEncodedRequest saveDataRequest(final String url) {
        return new PostUrlEncodedRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Ваш запрос отправлен", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        btnSend.setVisibility(View.VISIBLE);
    }

}
