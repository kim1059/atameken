package kz.npp.nppapp.fragments;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.ChooseFileType;
import kz.npp.nppapp.adapters.FilesAdapter;
import kz.npp.nppapp.models.Dictionary;
import kz.npp.nppapp.models.File;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.UploadAsyncTask;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFilesFragment extends Fragment {

    @BindView(R.id.llt_choose_file)
    LinearLayout lltChooseFile;
    @BindView(R.id.tv_file)
    TextView tvFile;
    @BindView(R.id.llt_choose_type)
    LinearLayout lltChooseType;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.btn_upload_file)
    Button btnUploadFile;
    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private String filePath;
    private String fileName;
    private String docName;
    private String docCode;
    private String storageParentId;

    private ArrayAdapter<File> adapter;
    private List<File> fileList = new ArrayList<>();

    private final int FILE_SELECT_CODE = 100;
    private final int FILE_TYPE_CODE = 200;

    private SharedPreferences shared;
    private List<Dictionary> dictionaryList = new ArrayList<>();

    private boolean isFilePicked;
    private boolean isTypePicked;

    public MyFilesFragment() {
        // Required empty public constructor
    }

    public static MyFilesFragment newInstance() {
        MyFilesFragment fragment = new MyFilesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_files, container, false);
        ButterKnife.bind(this, rootView);
        shared = PreferenceManager.getDefaultSharedPreferences(getActivity());

        adapter = new FilesAdapter(getActivity(), R.layout.fragment_my_files,fileList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        getStorageParentId();
        return rootView;
    }

    @Override
    public void onResume() {
        getFiles();
        super.onResume();
    }

    public void resumeFromUpload() {
        getFiles();
        tvFile.setText("Выберите файл");
        isFilePicked = false;
        tvType.setText("Выберите тип документа");
        isTypePicked = false;

        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    filePath = Utils.getPath(
                            getActivity(), uri);
                    if (filePath != null) {
                        fileName = filePath.substring(filePath.lastIndexOf("/") + 1).trim();
                    }
                    //filePath = getRealPathFromURI(uri);
                    //file = new File(getRealPathFromURI(uri));
                    tvFile.setText(uri.getPath());
                    isFilePicked = true;
                }
                break;
            case FILE_TYPE_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    docName = data.getStringExtra("name");
                    docCode = data.getStringExtra("code");
                    tvType.setText(docName);
                    isTypePicked = true;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.llt_choose_file)
    public void getImage () {
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("image/*");
        try {
            startActivityForResult(intent, FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.llt_choose_type)
    public void chooseFileType() {
        Intent intent = new Intent(getActivity(), ChooseFileType.class);
        startActivityForResult(intent, FILE_TYPE_CODE);
    }

    @OnClick(R.id.btn_upload_file)
    public void uploadImage () {
        if (!isFilePicked) {
            Toast.makeText(getActivity(), "Выберите файл", Toast.LENGTH_LONG).show();
            return;
        }
        if (!isTypePicked) {
            Toast.makeText(getActivity(), "Выберите тип файла", Toast.LENGTH_LONG).show();
            return;
        }
        getTempFilePath();
    }

    private void getStorageParentId() {
        GetRequest request = getParentIdRequest(Constants.STORAGE_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getFiles() {
        fileList.clear();
        showProgress();
        ArrayList<String> list = new ArrayList<String>();
        list.add("user");

        String uuid;
        if (Constants.isNpp) {
            uuid = "8bc289a4-4286-49db-8531-6792c762ff86";
        } else {
            uuid = "9c006d80-1576-4807-a471-297da20fcf64";
        }
        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='"+uuid+"' AND user like'"+shared.getString(Constants.MAIL, "")+"'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getTempFilePath() {
        GetRequest request = getTempFileRequest(Constants.START_UPLOAD_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void uploadFile(String path) {
        UploadAsyncTask uploadTask = new UploadAsyncTask(getActivity(), MyFilesFragment.this);
        uploadTask.execute(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""), fileName+"-file.png",
                path, filePath, storageParentId, docCode, docName);
    }

    private GetRequest getTempFileRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            String filePath;
                            JSONObject res = new JSONObject(response);

                            if (res.getString("errorCode").equalsIgnoreCase("0")) {
                                filePath = res.getString("file");
                                uploadFile(filePath);
                            } else {
                                Toast.makeText(getActivity(), "Не удалось создать временный файл", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }


    private GetRequest getParentIdRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0;i<jsonArray.length();i++) {
                                JSONObject parentObject = jsonArray.getJSONObject(i);
                                if (parentObject.getString("name").equalsIgnoreCase("Мои документы"))
                                    storageParentId = parentObject.getString("identifier");
                            }
                            if (jsonArray.length() == 0){
                                Toast.makeText(getActivity(), "Storage parent id is missing", Toast.LENGTH_LONG).show();
                                return;
                            }

                            //getFiles();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getFilesRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response", response);

                            hideProgress();
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            File file = new File();

                            for (int i=0;i<data.length();i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("date_time")){
                                    file.setDate(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("link_to_file")){
                                    file.setFileName(object.getString("value"));
                                    file.setUrl(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("type_doc")){
                                    file.setName(object.getString("value"));
                                    fileList.add(file);
                                }
                            }

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

   /* private String getRealPathFromURI(Uri uri) {
        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if(cursor == null){
            path = uri.getPath();
        }
        else{
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }
*/
}
