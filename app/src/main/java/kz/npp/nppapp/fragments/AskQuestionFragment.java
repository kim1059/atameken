package kz.npp.nppapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.npp.nppapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AskQuestionFragment extends Fragment {


    public AskQuestionFragment() {
        // Required empty public constructor
    }

    public static AskQuestionFragment newInstance() {
        AskQuestionFragment fragment = new AskQuestionFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ask_question, container, false);
    }

}
