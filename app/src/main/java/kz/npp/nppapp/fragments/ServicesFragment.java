package kz.npp.nppapp.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.CertificateSearch;
import kz.npp.nppapp.activities.CompanySearchActivity;
import kz.npp.nppapp.activities.OprosWebViewActivity;
import kz.npp.nppapp.activities.ServicesListActivity;
import kz.npp.nppapp.adapters.ServicesAdapter;
import kz.npp.nppapp.models.Service;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private ArrayAdapter<Service> adapter;
    private List<Service> servicesList = new ArrayList<>();
    private SharedPreferences shared;
    private Unbinder unbinder;

    public ServicesFragment() {
        // Required empty public constructor
    }

    public static ServicesFragment newInstance() {
        ServicesFragment fragment = new ServicesFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_services, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        shared = PreferenceManager.getDefaultSharedPreferences(getActivity());

        showProgress();
        /*get list of services*/
        getServices();

        adapter = new ServicesAdapter(getActivity(), R.layout.fragment_services,servicesList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (servicesList.get(position).getFormId() != null) {
                    startWebView(servicesList.get(position).getId());
                } else if (servicesList.get(position).getSystemReceiver().equalsIgnoreCase("1")) {
                    if (servicesList.get(position).getName().equalsIgnoreCase("Поиск сертификатов")) {
                        Intent intent = new Intent(getActivity(), CertificateSearch.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), CompanySearchActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Constants.FROM_INFO = false;
                    Intent intent = new Intent(getActivity(), ServicesListActivity.class);
                    intent.putExtra("service_num", servicesList.get(position).getServiceNum());
                    intent.putExtra("service", servicesList.get(position));
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getServices() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("level");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='e6fc184a-8258-4be1-bfbd-ed42b132ef77' AND level LIKE '0'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getServiceData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid, uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getServiceData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getActivity(), "Услуг пока нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getDataRequest(final String url, final String uuid) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            JSONObject name = null;
                            JSONObject num = null;
                            JSONObject image = null;
                            JSONObject receiver = null;
                            JSONObject formId = null;

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equals("status_service")) {
                                    if (!object.getString("key").equalsIgnoreCase("1"))
                                        return;
                                } else if (object.getString("id").equals("name_service")) {
                                    name = object;
                                } else if (object.getString("id").equals("num_service")) {
                                    num = object;
                                } else if (object.getString("id").equals("image2")) {
                                    image = object;
                                } else if (object.getString("id").equals("system_receiver")) {
                                    receiver = object;
                                } else if (object.getString("id").equals("form_id")) {
                                    formId = object;
                                }
                            }

                            Service service = new Service();
                            service.setId(uuid);
                            if (name != null) {
                                service.setName(name.getString("value"));
                            }
                            if (num != null) {
                                service.setServiceNum(num.getString("value"));
                            }
                            if ((receiver != null) && receiver.has("key")) {
                                service.setSystemReceiver(receiver.getString("key"));
                            } else {
                                service.setSystemReceiver("100");
                            }
                            if (image != null && image.has("key"))
                                service.setImageUrl(image.getString("key"));
                            if (formId != null) {
                                if (formId.has("value") && !TextUtils.isEmpty(formId.getString("value")))
                                    service.setFormId(formId.getString("value"));
                            }

                            servicesList.add(service);
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private void startWebView(String uuid) {
        String servUrl;
        if (Constants.isNpp)
            servUrl = "http://services.palata.kz/services/auto_service2.php?";
        else
            servUrl = "http://npp.arta.pro/services/auto_service2.php?";
        String url = servUrl +
                "uuid="+uuid+ "&" +
                "user="+shared.getString(Constants.LOGIN, "")+"&pass="+shared.getString(Constants.PASS, "")+"";
        Intent intent = new Intent(getActivity(), OprosWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

}
