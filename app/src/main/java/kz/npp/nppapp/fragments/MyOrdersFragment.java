package kz.npp.nppapp.fragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.MyOrderDetailsActivity;
import kz.npp.nppapp.adapters.OrdersAdapter;
import kz.npp.nppapp.models.Dictionary;
import kz.npp.nppapp.models.Order;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.utils.Utils;
import kz.npp.nppapp.volley.GetRequest;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrdersFragment extends Fragment {

    @BindView(R.id.lv_list)
    ListView lvList;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R.id.llt_sort)
    LinearLayout lltSort;
    @BindView(R.id.tv_sort_name)
    TextView tvSortName;
    @BindView(R.id.tv_status_name)
    TextView tvStatusName;
    @BindView(R.id.llt_status_filter)
    LinearLayout lltStatusFilter;
    @BindView(R.id.edt_date_from)
    EditText edtDateFrom;
    @BindView(R.id.edt_date_to)
    EditText edtDateTo;
    @BindView(R.id.tv_set_status)
    TextView tvSetStatus;

    ImageView ivDateDesc, ivDateAsc, ivStatus1, ivStatus2;
    ImageView ivNew, ivInProgress, ivDone, ivAccepted, ivAll;

    final int ORDER_DATE_DESC = 1;
    final int ORDER_DATE_ASC = 2;
    final int ORDER_STATUS_1 = 3;
    final int ORDER_STATUS_2 = 4;
    int order = 1;

    private ArrayAdapter<Order> adapter;
    private List<Order> ordersList = new ArrayList<>();
    private Unbinder unbinder;
    private SharedPreferences shared;
    private HashMap<String, String> statusMap;
    private boolean isDateFrom;
    private String dateFrom;
    private String dateTo;
    private String status = "";
    private int statusNum = 0;

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    public static MyOrdersFragment newInstance() {
        MyOrdersFragment fragment = new MyOrdersFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_orders, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        shared = PreferenceManager.getDefaultSharedPreferences(getActivity());

        tvSortName.setText("Дата (сначала новые)");
        tvStatusName.setText("Все");
        adapter = new OrdersAdapter(getActivity(), R.layout.fragment_my_orders,ordersList);
        lvList.setAdapter(adapter);

        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), MyOrderDetailsActivity.class);
                intent.putExtra("order", ordersList.get(position));
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        showProgress();
        getDictionary();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgress();
    }

    @OnClick(R.id.llt_sort)
    public void doSort() {
        // create a Dialog component
        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_sort_order);

        LinearLayout lltDateDes = (LinearLayout) dialog.findViewById(R.id.llt_date_desc);
        LinearLayout lltDateAsc = (LinearLayout) dialog.findViewById(R.id.llt_date_asc);

        ivDateDesc = (ImageView) dialog.findViewById(R.id.iv_date_desc);
        ivDateAsc = (ImageView) dialog.findViewById(R.id.iv_date_asc);

                /*put click on right position*/
        selectCurrentOrder();

        lltDateDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order = ORDER_DATE_DESC;
                selectCurrentOrder();

                dialog.dismiss();
                ordersList.clear();
                adapter.notifyDataSetChanged();
                getOrders(order);
            }
        });
        lltDateAsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order = ORDER_DATE_ASC;
                selectCurrentOrder();

                dialog.dismiss();
                ordersList.clear();
                adapter.notifyDataSetChanged();
                getOrders(order);
            }
        });
        dialog.show();
    }

    @OnClick(R.id.llt_status_filter)
    public void doFilter() {
        // create a Dialog component
        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_filter_status);

        LinearLayout lltAll = (LinearLayout) dialog.findViewById(R.id.llt_all);
        LinearLayout lltNew = (LinearLayout) dialog.findViewById(R.id.llt_new);
        LinearLayout lltAccepted = (LinearLayout) dialog.findViewById(R.id.llt_accepted);
        LinearLayout lltInProgress = (LinearLayout) dialog.findViewById(R.id.llt_in_progress);
        LinearLayout lltDone = (LinearLayout) dialog.findViewById(R.id.llt_done);

        ivNew = (ImageView) dialog.findViewById(R.id.iv_new);
        ivInProgress = (ImageView) dialog.findViewById(R.id.iv_in_progress);
        ivAccepted = (ImageView) dialog.findViewById(R.id.iv_accepted);
        ivDone = (ImageView) dialog.findViewById(R.id.iv_done);
        ivAll = (ImageView) dialog.findViewById(R.id.iv_all);

                /*put click on right position*/
        selectCurrentFilterOrder();

        lltAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 0;
                selectCurrentFilterOrder();

                dialog.dismiss();
            }
        });
        lltNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 1;
                selectCurrentFilterOrder();

                dialog.dismiss();
            }
        });
        lltAccepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 2;
                selectCurrentFilterOrder();

                dialog.dismiss();
            }
        });
        lltInProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 3;
                selectCurrentFilterOrder();

                dialog.dismiss();
            }
        });
        lltDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusNum = 4;
                selectCurrentFilterOrder();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void hideAllOkInDialog() {
        ivDateAsc.setVisibility(View.GONE);
        ivDateDesc.setVisibility(View.GONE);
    }

    private void selectCurrentOrder() {
        hideAllOkInDialog();
        if (order == ORDER_DATE_DESC) {
            tvSortName.setText("Дата (сначала новые)");
            ivDateDesc.setVisibility(View.VISIBLE);
        } else if (order == ORDER_DATE_ASC) {
            tvSortName.setText("Дата (сначала старые)");
            ivDateAsc.setVisibility(View.VISIBLE);
        }
    }

    private void hideAllOkInFilterDialog() {
        ivAll.setVisibility(View.GONE);
        ivNew.setVisibility(View.GONE);
        ivAccepted.setVisibility(View.GONE);
        ivInProgress.setVisibility(View.GONE);
        ivDone.setVisibility(View.GONE);
    }

    private void selectCurrentFilterOrder() {
        hideAllOkInFilterDialog();
        if (statusNum == 0) {
            tvStatusName.setText("Все");
            ivAll.setVisibility(View.VISIBLE);
        }else if (statusNum == 1) {
            tvStatusName.setText("Новые");
            ivNew.setVisibility(View.VISIBLE);
        } else if (statusNum == 2) {
            tvStatusName.setText("Принятые в работу");
            ivAccepted.setVisibility(View.VISIBLE);
        } else if (statusNum == 3) {
            tvStatusName.setText("В работе");
            ivInProgress.setVisibility(View.VISIBLE);
        } else if (statusNum == 4) {
            tvStatusName.setText("Исполненные");
            ivDone.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.edt_date_from)
    public void setDateFrom() {
        isDateFrom = true;
        Utils.hideSoftKeyboard(getActivity());
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @OnClick(R.id.edt_date_to)
    public void setDateTo() {
        isDateFrom = false;
        Utils.hideSoftKeyboard(getActivity());
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @OnClick(R.id.tv_set_status)
    public void setStatus() {
        if (statusNum != 0)
            status = " AND status_anketa.DATE="+statusNum;
        else
            status = "";
        if (TextUtils.isEmpty(edtDateFrom.getText().toString()) &&
                TextUtils.isEmpty(edtDateTo.getText().toString())) {
            getOrders(order);
        } else if (TextUtils.isEmpty(edtDateFrom.getText().toString())) {
            Toast.makeText(getActivity(), "нужно указать обе даты или не указывать даты", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(edtDateTo.getText().toString())) {
            Toast.makeText(getActivity(), "нужно указать обе даты или не указывать даты", Toast.LENGTH_LONG).show();
        } else {
            status += " AND date.DATE>'"+dateFrom +"' AND date.DATE<'"+dateTo+"'";
            getOrders(order);
        }
    }

    private void getOrders(int order) {
        ordersList.clear();
        adapter.notifyDataSetChanged();

        String sortBy = "";
        if (order == ORDER_DATE_DESC) {
            sortBy = "date.DATE DESC";
        } else if (order == ORDER_DATE_ASC) {
            sortBy = "date.DATE ASC";
        }
        ArrayList<String> list = new ArrayList<String>();
        list.add("client");
        list.add("date.DATE");
        if (!TextUtils.isEmpty(status) && statusNum != 0) {
            list.add("status_anketa.DATE");
        }

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='582ce05d-5b10-4639-bd0b-865993061b86' " +
                    "AND client LIKE '"+shared.getString(Constants.USER_ID, "")+"'"+status+" ORDER BY "+sortBy+"");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        Log.e("allinfo myorders",allInfo.toString());
        PostRequest request = myOrdersRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getOrderData(String uuid) {
        GetRequest request = getOrderDataRequest(Constants.GET_DATA_URL + uuid, uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getDictionary() {
        GetRequest request = getDictionaryRequest(Constants.DICTIONARY_URL + "status_anketa");
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private GetRequest getDictionaryRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();
                            Log.e("response", response);
                            JSONObject res = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(res.getString("items"));
                            statusMap = new HashMap<String, String>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONArray valuesArray = new JSONArray(jsonObject.getString("values"));
                                Dictionary dictionary = new Dictionary();

                                JSONObject valueObject1 = valuesArray.getJSONObject(0);
                                JSONObject valueObject2 = valuesArray.getJSONObject(1);
                                if (valueObject1.getString("value").length() > 1) {
                                    dictionary.setName(valueObject1.getString("value"));
                                    dictionary.setCode(valueObject2.getString("value"));
                                    statusMap.put(valueObject2.getString("value"), valueObject1.getString("value"));
                                } else {
                                    dictionary.setCode(valueObject1.getString("value"));
                                    dictionary.setName(valueObject2.getString("value"));
                                    statusMap.put(valueObject1.getString("value"), valueObject2.getString("value"));
                                }

                            }

                            getOrders(order);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private PostRequest myOrdersRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);

                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getOrderData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getActivity(), "Заявок нет", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private GetRequest getOrderDataRequest(final String url, final String uuid) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response getOrderData", response);
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            Order order = new Order();
                            order.setOrderUuid(uuid);
                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("date")) {
                                    order.setDate(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("application_id")) {
                                    order.setId(object.getString("value"));
                                } else if (object.getString("id").equalsIgnoreCase("num_zayavka")) {
                                    order.setApplicationNum(object.getString("value"));
                                    order.setOrderName("Заявка " + order.getApplicationNum());
                                } else if (object.getString("id").equalsIgnoreCase("executor") &&
                                        object.has("key")) {
                                    order.setExecutorId(object.getString("key"));
                                } else if (object.getString("id").equalsIgnoreCase("name_service")) {
                                    order.setServiceName(object.getString("value"));
                                } else if (object.getString("id").equalsIgnoreCase("status_anketa")) {
                                    order.setStatus(statusMap.get(object.getString("key")));
                                }
                            }
                            ordersList.add(order);

                            adapter.sort(new Comparator<Order>() {
                                public int compare(Order arg0, Order arg1) {
                                    if (arg0 != null && arg1 != null &&
                                            arg0.getApplicationNum() != null && arg1.getApplicationNum() != null) {
                                        return Integer.valueOf(arg1.getApplicationNum()).compareTo(Integer.valueOf(arg0.getApplicationNum()));
                                    } else {
                                        return -1;
                                    }
                                }
                            });
                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy hh:mm", cal).toString();
        return date;
    }

    private void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
    }

    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            //dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            // Create a new instance of DatePickerDialog and return it
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar c = Calendar.getInstance();
            c.set(year, month, day);

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            String formattedDate = sdf.format(c.getTime());

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String formattedDate2 = sdf2.format(c.getTime());

            if (isDateFrom) {
                dateFrom = formattedDate2;
                edtDateFrom.setText(formattedDate);
            } else {
                dateTo = formattedDate2;
                edtDateTo.setText(formattedDate);
            }

        }
    }

    /*@Override
    public void returnDate(String date, String orderDate) {
        if (isDateFrom) {
            this.dateFrom = orderDate;
            edtDateFrom.setText(date);
        } else {
            this.dateTo = orderDate;
            edtDateTo.setText(date);
        }

    }*/

    /*private void getOrders(int order) {
        String sortBy = "";
        if (order == ORDER_DATE_DESC) {
            sortBy = "&sort_by=dateStartService&direction=desc";
        } else if (order == ORDER_DATE_ASC) {
            sortBy = "&sort_by=dateStartService&direction=asc";
        }

        Log.e("url", Constants.MY_ORDERS_URL + shared.getString(Constants.USER_IIN, "") + sortBy + status);
        Log.e("xauth", Constants.XAUTHKEY);
        GetRequest request = getDataRequest(Constants.MY_ORDERS_URL + shared.getString(Constants.USER_IIN, "") + sortBy + status);
        request.setXauthKey(Constants.XAUTHKEY);
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }*/
    /*private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            ordersList.clear();
                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("object"));

                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                Order order = new Order();
                                String date = getDate(object.getLong("dateStartService"));

                                order.setId(object.getString("id"));
                                if (object.getString("applicationNumber").equalsIgnoreCase("null"))
                                    order.setApplicationNum("(без номера)");
                                else
                                    order.setApplicationNum(object.getString("applicationNumber"));
                                order.setDate(date);
                                order.setServiceName(object.getString("serviceName"));
                                order.setServiceNum(object.getString("serviceNumber"));
                                order.setOrderName("Заявка " + order.getApplicationNum());
                                order.setExecutorId(object.getString("executorId"));
                                order.setStatus(statusMap.get(object.getString("status")));
                                order.setExecutorHomePhone(
                                        object.getString("executorHomePhone").equalsIgnoreCase("null") ? "" : object.getString("executorHomePhone"));
                                ordersList.add(order);
                            }

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }*/

    /*private void getOrders() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("name_service");

        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("query", "where uuid='582ce05d-5b10-4639-bd0b-865993061b86' and name_service like '%'");
            allInfo.put("parameters", new JSONArray(list));
            allInfo.put("searchInRegistry", "true");
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = searchRequest(Constants.SEARCH_URL);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void getOrderData(String uuid) {
        GetRequest request = getDataRequest(Constants.GET_DATA_URL + uuid);
        request.setCredentials(shared.getString(Constants.LOGIN, ""), shared.getString(Constants.PASS, ""));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
    }*/

    /*private PostRequest searchRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("response",response);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                getOrderData(jsonObject.getString("dataUUID"));
                            }
                            if (jsonArray.length() == 0) {
                                hideProgress();
                                Toast.makeText(getActivity(), "Заказов нет", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }*/

    /*private GetRequest getDataRequest(final String url) {
        return new GetRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgress();

                            Log.e("response", response);

                            JSONObject res = new JSONObject(response);
                            JSONArray data = new JSONArray(res.getString("data"));

                            Order order = new Order();
                            for (int i=0; i<data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                if (object.getString("id").equalsIgnoreCase("date")){
                                    order.setDate(object.getString("value"));
                                }
                                if (object.getString("id").equalsIgnoreCase("name_service") &&
                                        !TextUtils.isEmpty(object.getString("value"))) {
                                    order.setServiceName(object.getString("value"));
                                    order.setOrderName("Заявка " + ordersList.size());
                                    ordersList.add(order);
                                }
                            }

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            hideProgress();
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Toast.makeText(getActivity(), "Network error", Toast.LENGTH_LONG).show();
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }
*/

}
