package kz.npp.nppapp.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;
import kz.npp.nppapp.activities.LoginActivity;
import kz.npp.nppapp.activities.ReportProblemActivity;
import kz.npp.nppapp.utils.Constants;
import kz.npp.nppapp.volley.PostRequest;
import kz.npp.nppapp.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private SharedPreferences shared;
    private SharedPreferences.Editor editor;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        shared =  PreferenceManager.getDefaultSharedPreferences(getActivity());

        return rootView;
    }

    @OnClick(R.id.llt_report_problem)
    public void openReport() {
        Intent intent = new Intent(getActivity(), ReportProblemActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llt_exit)
    public void exit() {
        removeGcmToken();
        editor = shared.edit();
        editor.clear();
        editor.apply();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    private void removeGcmToken() {
        String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        JSONObject allInfo = new JSONObject();
        try {
            allInfo.put("user", shared.getString(Constants.USER_ID, ""));
            allInfo.put( "deviceId", android_id);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неправильные json данные", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        PostRequest request = removeGcmRequest(Constants.REMOVE_TOKEN_URL);
        request.setXauthKey(Constants.XAUTHKEY);
        request.setData(allInfo.toString());
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);

    }

    private PostRequest removeGcmRequest(final String url) {
        return new PostRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("remove gcm", response);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getMessage() != null)
                            Log.e("registration", error.getMessage());
                    }
                });
    }

}
