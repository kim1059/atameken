package kz.npp.nppapp.fragments;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.npp.nppapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    public ContactsFragment() {
        // Required empty public constructor
    }

    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick(R.id.tv_call_any)
    public void callAny() {
        doCall("597960");
    }

    @OnClick(R.id.tv_call_almaty)
    public void callAlmaty() {
        doCall("2597960");
    }

    @OnClick(R.id.tv_call_kz)
    public void callKz() {
        doCall("88000808010");
    }

    @OnClick(R.id.tv_call_skype)
    public void callSkype() {
        doSkype("palata_kz");
    }

    private void doCall(String tel) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" +tel));
        startActivity(callIntent);
    }

    public void doSkype(String user) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("skype:" + user));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.e("SKYPE CALL", "Skype failed", e);
        }

    }

}
